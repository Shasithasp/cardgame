﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardManger : Singleton<CardManger>
{
    public Dropdown myDropdown;
    public Transform cardInitTransform;
    public GameObject panel;
    public GameObject cardPrefab;
    public List<GameObject> playerCardsPlaceHolderList = new List<GameObject>();
    public List<GameObject> OpponentCardsPlaceHolderList = new List<GameObject>();
    public List<SerializableCard> cards = new List<SerializableCard>();
    public List<Card> CardList = new List<Card>();
    public List<GameObject> playerCardsList = new List<GameObject>();
    public List<GameObject> OpponentCardsList = new List<GameObject>();


    public enum CardColor
    {
        Red,
        Orange,
        Yellow,
        Green,
        Black,
        Blue,
        Purple,
        Silver
    }

    public enum Type1
    {
        Male,
        Female,
        Animal,
        Monster,
        Hero,
        Villain,
        Place,
        Prop
    }

    public enum Type2
    {
        Decepticon,
        Autobot,
        Human
    }

    public enum Type3
    {
        Villain,
        Hero,
        Vehicle
    }


    [Serializable]
    public class SerializableCard
    {
        public int id;
        public CardColor color;
        public Type1 type1;
        public Type2 type2;
        public Type3 type3;
        public int value;
        public int power;
        public string bonusType;
        public Sprite sprite;
        public bool isDirty = false;
    }

    // Use this for initialization
    private void Start()
    {
        myDropdown.onValueChanged.AddListener(delegate { myDropdownValueChangedHandler(myDropdown); });

        cards.Add(new SerializableCard());
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private void Destroy()
    {
        myDropdown.onValueChanged.RemoveAllListeners();
    }

    private void myDropdownValueChangedHandler(Dropdown target)
    {
        //Debug.Log("selected: " + target.value);
        switch (target.value)
        {
            case 0:
                
                //   playerCardsPlaceHolderList[i].GetComponent<Image>().sprite = cards[i].sprite;
                for (var i = 0; i < 4; i++)
                {
                    int random = UnityEngine.Random.Range(0, 12);
                    var go = Instantiate(cardPrefab, Vector2.zero, Quaternion.identity) as GameObject;
                    go.transform.SetParent(panel.transform.GetChild(i).transform);
                    //    go.GetComponent<CardS>().id = cards[i].id;

                    go.GetComponent<CardS>().value = cards[i].value;
                    //   go.GetComponent<CardS>().color = cards[i].color;
                    go.GetComponent<Image>().sprite = cards[i].sprite;
                    //  // go. .GetComponent<Card>().type1 = cards[i].type1;
                    ////   go. GetComponent<Card>().type2 = cards[i].type2;
                    //  // go. GetComponent<Card>().type3 = cards[i].type3;
                   //  go.transform.GetChild(1).  GetComponent<Image>().color =Color.green;

                    go.GetComponent<CardS>().power = cards[i].power;

                    //   //  go.GetComponent<Card>().sprite = cards[i].sprite;
                    //   //  go.GetComponent<CardNetwork>().GetComponent<Image>().sprite = cards[i].sprite;
                    //   go.transform.name = go.transform + "PlayerCards";

                    playerCardsList.Add(go);
                }
                StartCoroutine(AiCardLoad());


                break;
            case 1:
              
                break;
            case 2:
            {
                break;
            }
            case 3:


                break;

            case 4:
                foreach (var cardsplaceHolder in playerCardsPlaceHolderList)
                {
                    cardsplaceHolder.GetComponent<Image>().sprite = null;
                }
                break;
            case 5:
            {
                break;
            }
            default:
            {
                break;
            }
        }
    }

  
    public IEnumerator AiCardLoad()
    {
        for (var i = 0; i <4; i++)
        {
            yield return new WaitForSeconds(2f);
            int random = UnityEngine.Random.Range(0, 12);

            var go = Instantiate(cardPrefab, Vector2.zero, Quaternion.identity) as GameObject;
            go.transform.SetParent(panel.transform);
            go.GetComponent<Image>().sprite = cards[i+1].sprite;
          //  Debug.Log(i);
            go.GetComponent<CardS>().value = cards[i+1].value;
            // go.GetComponent<CardS>().color = cards[i].color;

            go.transform.name = go.transform + "OpPlayerCards";
            go.transform.SetParent(OpponentCardsPlaceHolderList[i].gameObject.transform);
            OpponentCardsList.Add(go);
            //  StartCoroutine(DrawCardsToPlayerIEn());
        }

        yield return new WaitForSeconds(1f);

    }

    public void SecondRoundCardLoad()
    {
         for (var i = 0; i < 4; i++)
                {
                    int random = UnityEngine.Random.Range(0, 12);
                    var go = Instantiate(cardPrefab, Vector2.zero, Quaternion.identity) as GameObject;
                    go.transform.SetParent(panel.transform.GetChild(i).transform);
                    //    go.GetComponent<CardS>().id = cards[i].id;

                    go.GetComponent<CardS>().value = cards[i+3].value;
                    //   go.GetComponent<CardS>().color = cards[i].color;
                    go.GetComponent<Image>().sprite = cards[i+3].sprite;
                    //  // go. .GetComponent<Card>().type1 = cards[i].type1;
                    ////   go. GetComponent<Card>().type2 = cards[i].type2;
                    //  // go. GetComponent<Card>().type3 = cards[i].type3;
                   //  go.transform.GetChild(1).  GetComponent<Image>().color =Color.green;

                    go.GetComponent<CardS>().power = cards[i+3].power;

                    //   //  go.GetComponent<Card>().sprite = cards[i].sprite;
                    //   //  go.GetComponent<CardNetwork>().GetComponent<Image>().sprite = cards[i].sprite;
                    //   go.transform.name = go.transform + "PlayerCards";

                    playerCardsList.Add(go);
                }
    }

    public void SecondRoundAICardLoadIngame()
    {
        StartCoroutine(SecondRoundAICardLoad());
    }
    public IEnumerator SecondRoundAICardLoad()
    {

        for (var i = 4; i < 7; i++)
        {
            yield return new WaitForSeconds(2f);
            int random = UnityEngine.Random.Range(0, 12);

            var go = Instantiate(cardPrefab, Vector2.zero, Quaternion.identity) as GameObject;
            go.transform.SetParent(panel.transform);
            go.GetComponent<Image>().sprite = cards[i + 1].sprite;
            //  Debug.Log(i);
            go.GetComponent<CardS>().value = cards[i + 1].value;
            // go.GetComponent<CardS>().color = cards[i].color;

            go.transform.name = go.transform + "OpPlayerCards";
            go.transform.SetParent(OpponentCardsPlaceHolderList[i].gameObject.transform);
            OpponentCardsList.Add(go);
            //  StartCoroutine(DrawCardsToPlayerIEn());
        }
        yield return new WaitForSeconds(1f);
    }
}

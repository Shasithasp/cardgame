﻿using UnityEngine;
using System.Collections;

public class CardS : MonoBehaviour {

    public Vector3 target;

    public float speed = 20f;
    public bool setTargetLocally = false;
    public int id;
    public float smooth = 270; //2.0F;
    public float tiltAngle = 30.0F;

    public CardHandler.CardColor color;
    public CardHandler.Type1 type1;
    public CardHandler.Type2 type2;
    public CardHandler.Type3 type3;
    // public CardHandler.CardValue value;
    public int value;
    public int power;
    public Sprite sprite;
    public bool isDirty = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public IEnumerator HighlightCard()
    {

        this.transform.gameObject.GetComponent<CanvasRenderer>().SetColor(Color.green);
        yield return new WaitForSeconds(0.05f);

        this.transform.gameObject.GetComponent<CanvasRenderer>().SetColor(Color.white);

    }
}

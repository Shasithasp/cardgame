﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour,IBeginDragHandler,IDragHandler,IEndDragHandler
{

    public static GameObject itemsBeingDragged;
    private Vector3 startPosition;
    private Transform startParent;
    public List<GameObject> targetPosList; 
    

    public void OnBeginDrag(PointerEventData eventData)
    {
        //itemsBeingDragged = gameObject;
        //startPosition = transform.position;
        //startParent = transform.parent;
        //GetComponent<CanvasGroup>().blocksRaycasts = false;
        Debug.Log("OnBeginDrag");
    }

    public void OnDrag(PointerEventData eventData)
    {
        Debug.Log("OnDrag");
        transform.position = Input.mousePosition;
        
       
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        itemsBeingDragged = null;

        if (transform.parent != startParent)
        {
       //  transform.position = startPosition;
         //  transform.position =eventData.selectedObject.transform.position;

         //   Debug.Log("OnEndDrag" + eventData.selectedObject.name);
        }
        if (eventData.IsPointerMoving())
        {
            Debug.Log("OnEndDrag" +"Pointer moving");
        }
    }
}

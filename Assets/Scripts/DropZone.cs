﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropZone : MonoBehaviour , IDropHandler,
    IPointerEnterHandler, IPointerExitHandler
{
    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("OnDrop to DropZone " + gameObject.name);
        var d = eventData.pointerDrag.GetComponent<Draggable>();
        if (d != null)
        {
            d.parentToReturnto = transform;

            Debug.Log("OnDrop to  DropZone slot  " + gameObject.transform.tag);
        }
        Debug.Log("OnDrop to  DropZone outside " + gameObject.transform.tag);
        switch (gameObject.transform.tag)
        {
            case "slot1":

                Debug.Log("OnDROPPPPPPPPPPPPPP  Card1");
                if (gameObject.tag == "slot1" || gameObject.tag == "slot2" ||
                    gameObject.tag == "slot3" || gameObject.tag == "slot4" ||
                    gameObject.tag == "slot5" || gameObject.tag == "slot6")
                {
                    Debug.Log("OnDROP  Card1");
                    CardLoaded(1, false);
                    CardLoaded(2, false);
                    CardLoaded(3, false);
                    CardLoaded(4, false);
                    CardLoaded(5, false);
                    CardLoaded(6, false);
                    CardLoaded(7, false);
                }
                break;
            case "slot2":
                Debug.Log("OnDROPPPPPPPPPPPPPP  Card1");
                if (gameObject.tag == "slot1" || gameObject.tag == "slot2" ||
                    gameObject.tag == "slot3" || gameObject.tag == "slot4" ||
                    gameObject.tag == "slot5" || gameObject.tag == "slot6")
                {
                    Debug.Log("OnDROP  Card1");
                    CardLoaded(1, false);
                    CardLoaded(2, false);
                    CardLoaded(3, false);
                    CardLoaded(4, false);
                    CardLoaded(5, false);
                    CardLoaded(6, false);
                    CardLoaded(7, false);
                }
                break;
            case "slot3":
                Debug.Log("OnDROPPPPPPPPPPPPPP  Card1");
                if (gameObject.tag == "slot1" || gameObject.tag == "slot2" ||
                    gameObject.tag == "slot3" || gameObject.tag == "slot4" ||
                    gameObject.tag == "slot5" || gameObject.tag == "slot6")
                {
                    Debug.Log("OnDROP  Card1");
                    CardLoaded(1, false);
                    CardLoaded(2, false);
                    CardLoaded(3, false);
                    CardLoaded(4, false);
                    CardLoaded(5, false);
                    CardLoaded(6, false);
                    CardLoaded(7, false);
                }
                break;
            case "slot4":
                Debug.Log("OnDROPPPPPPPPPPPPPP  Card1");
                if (gameObject.tag == "slot1" || gameObject.tag == "slot2" ||
                    gameObject.tag == "slot3" || gameObject.tag == "slot4" ||
                    gameObject.tag == "slot5" || gameObject.tag == "slot6")
                {
                    Debug.Log("OnDROP  Card1");
                    CardLoaded(1, false);
                    CardLoaded(2, false);
                    CardLoaded(3, false);
                    CardLoaded(4, false);
                    CardLoaded(5, false);
                    CardLoaded(6, false);
                    CardLoaded(7, false);
                }
                break;
            case "slot5":
                Debug.Log("OnDROPPPPPPPPPPPPPP  Card1");
                if (gameObject.tag == "slot1" || gameObject.tag == "slot2" ||
                    gameObject.tag == "slot3" || gameObject.tag == "slot4" ||
                    gameObject.tag == "slot5" || gameObject.tag == "slot6")
                {
                    Debug.Log("OnDROP  Card1");

                    foreach (var plCardHolder in CardHandler.Instance.playerCardsPlaceHolderList)
                    {
                        if (plCardHolder.GetComponentInChildren<Card>().id ==null)
                        {
                            
                        }
                    }

                    CardLoaded(1, false);
                    CardLoaded(2, false);
                    CardLoaded(3, false);
                    CardLoaded(4, false);
                    CardLoaded(5, false);
                    CardLoaded(6, false);
                    CardLoaded(7, false);
                }
                break;
            case "slot6":
                Debug.Log("OnDROPPPPPPPPPPPPPP  Card1");
                if (gameObject.tag == "slot1" || gameObject.tag == "slot2" ||
                    gameObject.tag == "slot3" || gameObject.tag == "slot4" ||
                    gameObject.tag == "slot5" || gameObject.tag == "slot6")
                {
                    Debug.Log("OnDROP  Card1");
                    CardLoaded(1, false);
                    CardLoaded(2, false);
                    CardLoaded(3, false);
                    CardLoaded(4, false);
                    CardLoaded(5, false);
                    CardLoaded(6, false);
                    CardLoaded(7, false);
                }
                break;
        }
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("OnDrop to " + this.gameObject.name);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
    }


    public void OnPointerExit(PointerEventData eventData)
    {
        //  Debug.Log("OnDrop to " + gameObject.name);
    }


    public void setCardDetails(Sprite imageSprite)
    {
        throw new NotImplementedException();
    }

    public void CardLoaded(int index, bool isloaded)
    {
        if (index == 1)
        {
            GameHandler.Instance.cardOneIsLoaded = isloaded;
        }
        if (index == 2)
        {
            GameHandler.Instance.cardTwoIsLoaded = isloaded;
        }
        if (index == 3)
        {
            GameHandler.Instance.cardThreeIsLoaded = isloaded;
        }
        if (index == 4)
        {
            GameHandler.Instance.cardFourIsLoaded = isloaded;
        }
        if (index == 5)
        {
            GameHandler.Instance.cardFiveIsLoaded = isloaded;
        }
        if (index == 6)
        {
            GameHandler.Instance.cardSixIsLoaded = isloaded;
        }
        if (index == 7)
        {
            GameHandler.Instance.cardSevenIsLoaded = isloaded;
        }
        //if (GameHandler.Instance.cardOneIsLoaded && GameHandler.Instance.cardTwoIsLoaded && GameHandler.Instance.cardThreeIsLoaded && GameHandler.Instance.cardFourIsLoaded)
        //{
        //    ReadyCards();
        //    Invoke("CheckHandSCore", 3f);
        //}
        //if ( GameHandler.Instance.cardFiveIsLoaded && GameHandler.Instance.cardSixIsLoaded && GameHandler.Instance.cardSevenIsLoaded)
        //{
        //    SecondRoundReadyCards();
        //}
    }


    public void OnDrag(PointerEventData eventData)
    {
        //var d = eventData.pointerDrag.GetComponent<Draggable>();
    }

  

    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("OnDrop to " + gameObject.name);
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CardInGame : SingletonPhoton<CardInGame>
{

    protected CardInGame() { } // guarantee this will be always a singleton only - can't use the constructor!

    public void Awake()
    {
        // in case we started this demo with the wrong scene being active, simply load the menu scene
        if (!PhotonNetwork.connected)
        {
         SceneManager.LoadScene(CardGameRoomMenu.SceneNameMenu);
        //    Application.LoadLevel(0);
            return;
        }
    }

    public void OnGUI()
    {
        if (GUI.Button((new Rect(10f,10f,100f,40f)), "Return to Lobby"))
        {
            PhotonNetwork.LeaveRoom();  // we will load the menu level when we successfully left the room
        }
    }

    public void OnMasterClientSwitched(PhotonPlayer player)
    {
      //  Debug.Log("OnMasterClientSwitched: " + player);
         
        //string message;
        //InRoomChat chatComponent = GetComponent<InRoomChat>();  // if we find a InRoomChat component, we print out a short message
        //OpenChat chatComp = GetComponent<OpenChat>();

        //if (chatComponent != null)
        //{
        //    // to check if this client is the new master...
        //    if (player.isLocal)
        //    {
        //        message = "You are Master Client now.";
        //    }
        //    else
        //    {
        //        message = player.name + " is Master Client now.";
        //    }

        //    chatComp.AddLine(message);
        //    chatComponent.AddLine(message); // the Chat method is a RPC. as we don't want to send an RPC and neither create a PhotonMessageInfo, lets call AddLine()
        //}
    }

    public void OnLeftRoom()
    {
       // Debug.Log("OnLeftRoom (local)");

        // back to main menu
		SceneManager.LoadScene(CardGameRoomMenu.SceneNameMenu);
    }

    public void OnDisconnectedFromPhoton()
    {
       // Debug.Log("OnDisconnectedFromPhoton");

        // back to main menu
		SceneManager.LoadScene(CardGameRoomMenu.SceneNameMenu);
    }

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
       // Debug.Log("OnPhotonInstantiate " + info.sender);    // you could use this info to store this or react
    }

    public void OnPhotonPlayerConnected(PhotonPlayer player)
    {
     //   Debug.Log("OnPhotonPlayerConnected: " + player);
        //if (PhotonNetwork.playerList.Length == 2)
        //{
        //    PhotonNetwork.LoadLevel("Connecting");
        //}
        //else if (PhotonNetwork.playerList.Length == 1)
        //{
        //    Debug.Log("Not Enough PLayers");
          
        //}

    }

    public void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
      //  Debug.Log("OnPlayerDisconneced: " + player);
    }

    public void OnFailedToConnectToPhoton()
    {
     //   Debug.Log("OnFailedToConnectToPhoton");

        // back to main menu
		SceneManager.LoadScene(CardGameRoomMenu.SceneNameMenu);
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

public class GUIHandler : Singleton<GUIHandler>
{
   
    protected GUIHandler() { }

    [Header("NetWork")]
    public CardGameRoomMenu CardRoomMenu;

    public GameObject loginPanel;
    public GameObject roomPanel;
    public Text   statsText;

    public Image image;
    public Text colorName;
    public Text nameText;
    public Text GenderText;
    public Text DetailsText;
    private int countBlack;

    public int countgreen;
    public int countRed;
    public int countYellow;
    public int countOrange;
    public int countSilver;
    public int countPurple;
    public int countBlue;
    public Button ReadyButton;
	// Use this for initialization
  
    public void Login()
    {
        loginPanel.SetActive(false);
        CardRoomMenu.Connect();
        roomPanel.SetActive(true);
    }
    void Awake()
    {
        //if (PhotonNetwork.isMasterClient)
        //{
        //    PhotonNetwork.CreateRoom("Myroom", new RoomOptions() { maxPlayers = 2 }, null);
        //    Debug.Log("Inroom");
        //}
    }
    void Start()
    {
        //if (!PhotonNetwork.connected)
        //{
        //    loginPanel.SetActive(true);
        //    roomPanel.SetActive(false);
        //}
        //else
        //{
        //    loginPanel.SetActive(false);
        //    CardRoomMenu.Connect();
        //    roomPanel.SetActive(true);
        //}
    }

    public void ShowReadyBtn()
    {
        
        ReadyButton.gameObject.SetActive(true);
    }
	// Update is called once per frame
	void Update () {
	
	}
    public void OnGUI()
    {
        //if (GUI.Button((new Rect(10f, 10f, 100f, 40f)), "Return to Lobby"))
        //{
        //    PhotonNetwork.LeaveRoom();  // we will load the menu level when we successfully left the room
        //}
    }

    public void TwoReveledColorCount()
    {
        switch (GameHandler.Instance.TwoColorReveled)
        {
                case GameHandler.Color.Black:
                for (int i = 0; i < 7; i++)
                {
                    
                        if (CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>().GetComponent<Card>().color == CardHandler.CardColor.Black)
                        {
                            countBlack++;
                          
                        }
                   
                    
                }
                break;
                case GameHandler.Color.Silver:
                for (int i = 0; i < 7; i++)
                {

                    if (CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>().GetComponent<Card>().color == CardHandler.CardColor.Silver)
                    {
                        countSilver++;

                    }


                }
                break;
                case GameHandler.Color.Blue:
                for (int i = 0; i < 7; i++)
                {

                    if (CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>().GetComponent<Card>().color == CardHandler.CardColor.Blue)
                    {
                        countBlue++;

                    }


                }
                break;
                case GameHandler.Color.Red:
                for (int i = 0; i < 7; i++)
                {

                    if (CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>().GetComponent<Card>().color == CardHandler.CardColor.Red)
                    {
                        countRed++;

                    }


                }
                break;
                case GameHandler.Color.Yellow:
                for (int i = 0; i < 7; i++)
                {

                    if (CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>().GetComponent<Card>().color == CardHandler.CardColor.Yellow)
                    {
                        countYellow++;

                    }


                }
                break;
                case GameHandler.Color.Green:
                for (int i = 0; i < 7; i++)
                {

                    if (CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>().GetComponent<Card>().color == CardHandler.CardColor.Green)
                    {
                        countgreen++;

                    }


                }
                break;
                case GameHandler.Color.Purple:
                for (int i = 0; i < 7; i++)
                {

                    if (CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>().GetComponent<Card>().color == CardHandler.CardColor.Purple)
                    {
                        countPurple++;

                    }


                }
                break;
        }
    }

    public void setCardDetails(Sprite imageSprite, CardHandler.CardColor color, CardHandler.Type1 type1, CardHandler.Type2 type2)
    {
        image.sprite = imageSprite;
        colorName.text = color.ToString();
        nameText.text = imageSprite.name;
         GenderText.text = type1.ToString();
        DetailsText.text = type2.ToString();

    }

    public interface IGUIHandler
    {
        void setCardDetails(Sprite imageSprite, CardHandler.CardColor color, CardHandler.Type1 type1, CardHandler.Type2 type2);

    }


}

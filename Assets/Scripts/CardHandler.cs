﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

[Serializable]
public class CardHandler : Singleton<CardHandler>
{
    public Transform cardInitTransform;

    public GameObject cardPrefab;
    public List<GameObject> playerCardsPlaceHolderList = new List<GameObject>();
    public List<GameObject> OpponentCardsPlaceHolderList = new List<GameObject>();
    public List<SerializableCard> cards = new List<SerializableCard>();
    public List<Card> CardList = new List<Card>();
    public List<GameObject> playerCardsList = new List<GameObject>();
    public List<GameObject> OpponentCardsList = new List<GameObject>();
    public GameObject panel;
    public bool isHandReady;
    public bool loadedd;
    public delegate void loadcards();

    public static event loadcards ReadyCards;
    public enum CardColor
    {
        Red,
        Orange,
        Yellow,
        Green,
        Black,
        Blue,
        Purple,
        Silver
    }

    public enum Type1
    {
        Male,
        Female,
        Animal,
        Monster,
        Hero,
        Villain,
        Place,
        Prop
    }

    public enum Type2
    {
        Decepticon,
        Autobot,
        Human
    }

    public enum Type3
    {
        Villain,
        Hero,
        Vehicle
    }



    [Serializable]
    public class SerializableCard
    {
        public int id;
        public CardColor color;
        public Type1 type1;
        public Type2 type2;
        public Type3 type3;
        public int value;
        public int power;
        public string bonusType;
        public Sprite sprite;
        public bool isDirty = false;
    }

  
    public void Awake()
    {
        //// in case we started this demo with the wrong scene being active, simply load the menu scene
        //if (!PhotonNetwork.connected)
        //{
        //    SceneManager.LoadScene("Main");
        //    return;
        //}
    }

    public Dropdown myDropdown;

    private void Start()
    {
        myDropdown.onValueChanged.AddListener(delegate { myDropdownValueChangedHandler(myDropdown); });
    
        cards.Add(new SerializableCard());
    }

    private void Destroy()
    {
        myDropdown.onValueChanged.RemoveAllListeners();
    }
    public void SetCardTargetLocally(GameObject go, int id)
    {
        Debug.Log(go.name+"Game obejct name   photon view ID"+id);
        foreach (var item in PlayerHandler.Instance.playerList)
        {
            int indexx = 0;
            if (item.GetComponent<Player>().id == id)
            {
              //  go.GetComponent<Card>().target = CardHandler.Instance.OpponentCardsPlaceHolderList[2].transform.;//PlayerHandler.Instance.playerPlaceHolderList[item.GetComponent<Player>().position].transform.GetChild(item.transform.childCount - PlayerHandler.Instance.initialChildCount).transform.position;

                go.GetComponent<Card>().transform.SetParent(CardHandler.Instance.OpponentCardsPlaceHolderList[1].transform);
            //    go.transform.SetParent(item.transform);
                indexx++;

            }
        }
    }
    public IEnumerator ChangeSprite(GameObject go, float wait)
    {
        yield return new WaitForSeconds(wait);
        go.GetComponent<Image>().sprite = cards[go.GetComponent<Card>().id].sprite;

    

        //if (PhotonNetwork.isMasterClient )
        //{
        //    go.GetComponent<Image>().sprite = cards[go.GetComponent<Card>().id].sprite;
        //    Card comCard = new Card();
        //    string card = "";
        //    card = go.GetComponent<Image>().sprite.name;
        ////    go.transform.SetParent(OpponentCardsPlaceHolderList[i].gameObject.transform);
        //    OpponentCardsList.Add(go);
           
  
        //}
        ////if (count > 3)
        ////{

        ////   /// GameHandler.Instance.HandRanking();
        ////}
    }

    public void initializeSecondRoundCard()
    {
        for (int i = 0; i < 4; i++)
        {
            var go = PhotonNetwork.Instantiate(cardPrefab.name, Vector2.zero, Quaternion.identity, 0);
            go.transform.SetParent(panel.transform.GetChild(i).transform);
            go.GetComponent<CardNetwork>().GetComponent<Card>().id = cards[i+5].id;
            go.GetComponent<CardNetwork>().GetComponent<Card>().value = cards[i+5].value;
            go.GetComponent<CardNetwork>().GetComponent<Card>().color = cards[i+5].color;
            go.GetComponent<CardNetwork>().GetComponent<Card>().sprite = cards[i+5].sprite;

            // go.transform.GetChild(1).  GetComponent<Image>().color =Color.green;
            go.GetComponent<CardNetwork>().gameObject.GetComponent<Image>().sprite = cards[i+5].sprite;
            go.GetComponent<CardNetwork>().GetComponent<Card>().power = cards[i+5].power;

            //  go.GetComponent<Card>().sprite = cards[i].sprite;
            //  go.GetComponent<CardNetwork>().GetComponent<Image>().sprite = cards[i].sprite;
            go.transform.name = go.transform + "PlayerCards";
            if (i>-1 && i<3)
            {
                playerCardsList.Add(go);
            }
         //  
        }

    }
  
    private void myDropdownValueChangedHandler(Dropdown target)
    {
        //Debug.Log("selected: " + target.value);
        switch (target.value)
        {
            case 0:
                 
                    //   playerCardsPlaceHolderList[i].GetComponent<Image>().sprite = cards[i].sprite;
                for (int i=0;i<4;i++)
                {
                    var go = PhotonNetwork.Instantiate(cardPrefab.name, Vector2.zero, Quaternion.identity, 0);
                    go.transform.SetParent(  panel.transform.GetChild(i).transform);
                    go.GetComponent<CardNetwork>().GetComponent<Card>().id = cards[i].id;
                     go.GetComponent<CardNetwork>().GetComponent<Card>().value = cards[i].value;
                     go.GetComponent<CardNetwork>().GetComponent<Card>().color = cards[i].color;
                    go.GetComponent<CardNetwork>().GetComponent<Card>().sprite = cards[i].sprite;
                    go.GetComponent<CardNetwork>().GetComponent<Card>().type1 = cards[i].type1;
                    go.GetComponent<CardNetwork>().GetComponent<Card>().type2 = cards[i].type2;
                    go.GetComponent<CardNetwork>().GetComponent<Card>().type3 = cards[i].type3;
                   // go.transform.GetChild(1).  GetComponent<Image>().color =Color.green;
                    go.GetComponent<CardNetwork>().gameObject.GetComponent<Image>().sprite = cards[i].sprite;
                    go.GetComponent<CardNetwork>().GetComponent<Card>().power = cards[i].power;
                  
                    //  go.GetComponent<Card>().sprite = cards[i].sprite;
                    //  go.GetComponent<CardNetwork>().GetComponent<Image>().sprite = cards[i].sprite;
                    go.transform.name = go.transform + "PlayerCards";

                    playerCardsList.Add(go);
                    
                }
                StartCoroutine(WaitTillCards());

               

                break;
            case 1:
                
                break;
            case 2:
            {
              

                break;
            }
            case 3:
                OpponentCardLoad();


                break;

            case 4:
                foreach (var cardsplaceHolder in playerCardsPlaceHolderList)
                {
                    cardsplaceHolder.GetComponent<Image>().sprite = null;
                }
                break;
            case 5:
            {
                break;
            }
            default:
            {
                break;
            }
        }
    }

    public virtual void testmethod()
    {
        Debug.Log("Test Methods running ");
    }
    public void OpponentCardLoad()
    {
        for (var i = 0; i < 6; i++)
        {
            var go = PhotonNetwork.Instantiate(cardPrefab.name, Vector2.zero, Quaternion.identity, 0);
            go.transform.SetParent(panel.transform.GetChild(i).transform);
            go.GetComponent<Image>().sprite = cards[i].sprite;
            go.GetComponent<Card>().value = cards[i].value;
            go.GetComponent<Card>().color = cards[i].color;
       
            go.transform.name = go.transform + "OpPlayerCards";
            go.transform.SetParent(OpponentCardsPlaceHolderList[i].gameObject.transform);
            OpponentCardsList.Add(go);
          //  StartCoroutine(DrawCardsToPlayerIEn());
        }
    }

    public void SetDropdownIndex(int index)
    {
        myDropdown.value = index;
    }

  
    // Update is called once per frame
    private void FixedUpdate()
    {
        //if (playerCardsPlaceHolderList[0].GetComponentInChildren<Card>().gameObject== null)
        //{
        //    if (playerCardsPlaceHolderList[1].GetComponentInChildren<Card>().gameObject == null)
        //    {
        //        if (playerCardsPlaceHolderList[2].GetComponentInChildren<Card>().gameObject == null)
        //        {
        //            if (playerCardsPlaceHolderList[3].GetComponentInChildren<Card>().gameObject == null)
        //            {
        //                isHandReady = true;
        //            }
        //        }
        //    }
        //}
    }

    public IEnumerator WaitTillCards()
    {
     
      
        //Debug.Log("WaitUntil");

        while (playerCardsPlaceHolderList[0].transform.parent.childCount == 0)  //this.gameObject.transform.childCount > 0)
        {
            Debug.Log("has no child");
            yield return null;
        }
        while (playerCardsPlaceHolderList[1].transform.parent.childCount == 0)
        {
            Debug.Log("NOT hasChanged");
            yield return null;
        }
        while (playerCardsPlaceHolderList[2].transform.parent.childCount == 0)
        {
            Debug.Log("NOT hasChanged");
            yield return null;
        }
        while  (playerCardsPlaceHolderList[3].transform.parent.childCount == 0)
        {
            Debug.Log("NOThasChanged");
            yield return null;
        }


        if (playerCardsPlaceHolderList[0].gameObject.transform.parent.childCount >= 1 && playerCardsPlaceHolderList[1].gameObject.transform.parent.childCount >= 1 && playerCardsPlaceHolderList[2].gameObject.transform.parent.childCount >= 1 && playerCardsPlaceHolderList[3].gameObject.transform.parent.childCount >= 1)
        {
            loadedd = true;
            Debug.Log("Rooms Refreshed");
            yield return new WaitForSeconds(2f);
        }
       
            yield return null;
         
    }

   
    public void LoadedEachcard()
    {
        for (int i = 0; i < 4; i++)
        {
            if (playerCardsPlaceHolderList[i].GetComponentInChildren<Card>().gameObject != null)
            {
                loadedd = true;
            }
            
        }

    }
    
}

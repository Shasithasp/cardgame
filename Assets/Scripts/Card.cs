﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[Serializable]
public class Card : MonoBehaviour, IPointerEnterHandler,  IDropHandler,GUIHandler.IGUIHandler
{
    public Vector3 target;

    public float speed=20f;
    public bool setTargetLocally = false;
    public int id;
    public float smooth = 270; //2.0F;
    public float tiltAngle = 30.0F;

    public CardHandler.CardColor color;
    public CardHandler.Type1 type1;
    public CardHandler.Type2 type2;
    public CardHandler.Type3 type3;
   // public CardHandler.CardValue value;
    public int value;
    public int power;
    public Sprite sprite;
    public bool isDirty = false;
    public int randomNumber;
    public string bonusType;
    public AnimationCurve scaleCurve;
    public Image panelsDetails;
    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        float step = speed * Time.deltaTime * 3;
      transform.position = Vector3.MoveTowards(transform.position, target, step);
        //   transform.Rotate(Vector3.up, Time.deltaTime, Space.World);
        //transform.Rotate(Vector2.up*randomNumber);
      //  
    }

    public void Rotate()
    {
        Debug.Log("OnDrop to " + this.gameObject.GetComponent<Image>().sprite.name);
 
 
        //  panelsDetails.sprite = this.gameObject.GetComponent<Image>().sprite;
        //   transform.Rotate(Vector3.up, speed, Space.World);
        //   transform.localRotation = Quaternion.EulerAngles(0f, 360f, 0f);
        //  transform.Rotate(0, Time.deltaTime * 55f, 0);
        // (Vector2.up*randomNumber);
    }

    public IEnumerator Flip(Sprite endsprite)
    {
        float time = 0f;
        while (time<=1f)
        {
            float scale = scaleCurve.Evaluate(time);
            time = time + Time.deltaTime/0.005f;
            Vector3 localScale = transform.localScale;
            localScale.x = scale;
            transform.localScale = localScale;
            if (time>=0.5f)
            {
                this.sprite = endsprite;

            }
            yield return new WaitForSeconds(0.001f);

        }
    }
    public IEnumerator UnFlip(Sprite endsprite)
    {
        float time = 0f;
        while (time <= 1f)
        {
            float scale = scaleCurve.Evaluate(time);
            time = time + Time.deltaTime / 0.005f;
            Vector3 localScale = transform.localScale;
            localScale.x = scale;
            transform.localScale = localScale;
            if (time >= 0.5f)
            {
                this.sprite = endsprite;

            }
            yield return new WaitForSeconds(0.001f);

        }
    }

    public IEnumerator HighlightCard()
    {

        this.transform.gameObject.GetComponent<CanvasRenderer>().SetColor(Color.green);
        yield return new WaitForSeconds(0.05f);

        this.transform.gameObject.GetComponent<CanvasRenderer>().SetColor(Color.white);

    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("OnDrop to " + this.gameObject .name );// +this.gameObject.GetComponentInChildren<GameObject>().GetComponent<Image>().name );
      //  setCardDetails(this.gameObject.GetComponent<Image>().sprite);
        setCardDetails(this.gameObject.GetComponent<Image>().sprite, color , type1, type2);


    }


    //public void setCardDetails(Sprite imageSprite)
    //{
    //   // GameHandler.Instance.CardDetails.sprite = imageSprite;
    //}

    public void OnDrop(PointerEventData eventData)
    {

        if (this.gameObject.   transform.parent.childCount > 0)
        {
            Debug.Log("OnDrop to card " + this.gameObject.name);
        }
    }


    public void CardLoaded(int index, bool isloaded)
    {
        throw new NotImplementedException();
    }


    public void ReadyCards()
    {
        throw new NotImplementedException();
    }

   
   

   

    public void setCardDetails(Sprite imageSprite, CardHandler.CardColor color, CardHandler.Type1 type1, CardHandler.Type2 type2)
    {
        GUIHandler.Instance. image.sprite = imageSprite;
        GUIHandler.Instance.colorName.text = color.ToString();
        GUIHandler.Instance.nameText.text = imageSprite.name;
        GUIHandler.Instance.GenderText.text = type1.ToString();
        GUIHandler.Instance.DetailsText.text = type2.ToString();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameHandler : Singleton<GameHandler>, IPunTurnManagerCallbacks
{
    [SerializeField]
    public int Index { get; set; }
    public bool cardOneIsLoaded;
    public bool cardTwoIsLoaded;
    public bool cardThreeIsLoaded;
    public bool cardFourIsLoaded;

    public bool cardFiveIsLoaded;
    public bool cardSixIsLoaded;

    public bool cardSevenIsLoaded;

    public int CardIndex { get; set; }
    public Text scoreText;
    public Animator ScoreAnimator;
    public int scoreValue;
    public Text opScoreText;
    public Sprite defaultSprite;
    public int ScoreValueOp;
    public GameObject colorPanel;
    public Text timerText;
    private PunTurnManager turnManager;
    public Sprite Black;
    public Sprite Blue;
    public Sprite Yellow;
    public Sprite Orange;
    public Sprite Purple;
    public Sprite Red;
    public Sprite Green;
    public Sprite Silver;
    public List<Sprite> colorsList = new List<Sprite>();
    public bool isOneColorReveled;
    public bool isTwoColorReveled;
    public Button playerReveledColorbtn;
    public Button OPReveledColorbtn;
    public int countBlack;
    public int countgreen;
    public int countRed;
    public int countYellow;
    public int countOrange;
    public int countSilver;
    public int countPurple;
    public Color oneColorReveled;
    public Color TwoColorReveled;

    public Button oneClrRevelbtn;
    public Button TwoClrRevelbtn;
    public Button OponeClrRevelbtn;
    public Button OponeTwoClrRevelbtn;

    public Image CardDetails;

    [SerializeField] private Image localSelectionImage;
    public Color localSelection = Color.Black;
    public List<Card> arrangeCardsList = new List<Card>();
    public PhotonView photonView;
    public Button colorSelectBtn;
    public Text plCardCountTxt;
    public Text pltwoCardCountText;
    public Text OptwoCardCountText;
    public Text opCardCountTxt;
    [SerializeField] private Image remoteSelectionImage;
    public Color remoteSelection;

    private Sprite SelectionToSprite(Color hand)
    {
        switch (hand)
        {
            case Color.Black:
                return Black;
            case Color.Blue:
                return Blue;
            case Color.Yellow:
                return Yellow;
            case Color.Purple:
                return Purple;
            case Color.Orange:
                return Orange;
            case Color.Silver:
                return Silver;
            case Color.Red:
                return Red;
            case Color.Green:
                return Green;
        }

        return null;
    }

    public enum GameStates
    {
        GameInit,
        GameStart,
        GameColor,
        GameOver,
        GameWin,
        GameLost
    }

    public enum Color
    {
        Green,
        Yellow,
        Purple,
        Black,
        Silver,
        Blue,
        Orange,
        Red
    }

    public GameStates CurrentGameState = GameStates.GameInit;

    public delegate void ScoreChange(int val);

    private ScoreChange newChangecoreChange;

    public int ScoreValue
    {
        get { return scoreValue; }
        set
        {
            scoreValue = value;
            scoreText.text = scoreValue.ToString();
            ScoreAnimator.Play(1);
        }
    }

    private void Awake()
    {
        colorsList.Add(Orange);
        colorsList.Add(Green);
        colorsList.Add(Blue);
        colorsList.Add(Black);
        colorsList.Add(Purple);
        colorsList.Add(Red);
        colorsList.Add(Yellow);
    }

    private void Start()
    {
        colorPanel.gameObject.SetActive(false);
        CurrentGameState = GameStates.GameInit;
        turnManager = gameObject.AddComponent<PunTurnManager>();
        turnManager.TurnManagerListener = this;
        turnManager.TurnDuration = 15f;
        colorReveled();
        localSelection = Color.Black;
    }

    public void FixedUpdate()
    {
    }

    public void colorReveled()
    {
        if (PhotonNetwork.inRoom && PhotonNetwork.isMasterClient)
        {
            var random = Random.Range(1, 3);
            var randomColor = Random.Range(0, 7);
            Debug.Log("Random value : " + random);
            if (random == 1)
            {
                isOneColorReveled = true;
                photonView.RPC("OneColorcardReveled", PhotonTargets.AllBuffered, random);

                //    colorPanel.SetActive(true);
            }
            else
            {
                Debug.Log("Random value : " + random);
                isTwoColorReveled = true;

                photonView.RPC("TwoColorcardReveled", PhotonTargets.AllBuffered, randomColor);
            }
        }
    }

    [PunRPC]
    public void OneColorcardReveled(int colorval)
    {
        isOneColorReveled = true;
        oneClrRevelbtn.image.sprite = colorsList[colorval];
        OponeClrRevelbtn.image.sprite = colorsList[colorval];
        Debug.Log("OneColorcardReveled: " + OponeClrRevelbtn.image.sprite.name);
        switch (OponeClrRevelbtn.image.sprite.name)
        {
            case "Silver":
                oneColorReveled = Color.Silver;
                Debug.Log("Silver");
                break;
            case "Red":
                oneColorReveled = Color.Red;
                Debug.Log("Red");
                break;
            case "Purple":
                oneColorReveled = Color.Purple;
                Debug.Log("Purple");
                break;
            case "Green":
                oneColorReveled = Color.Green;
                Debug.Log("Green");
                break;
            case "Blue":
                oneColorReveled = Color.Blue;
                Debug.Log("Blue");
                break;
            case "Yellow":
                oneColorReveled = Color.Yellow;
                Debug.Log("Yellow");
                break;
            case "Black":
                oneColorReveled = Color.Black;
                Debug.Log("Black");
                break;
        }
    }

    [PunRPC]
    public void TwoColorcardReveled(int index)
    {
        isTwoColorReveled = true;
        playerReveledColorbtn.gameObject.SetActive(true);
        OPReveledColorbtn.gameObject.SetActive(true);


        playerReveledColorbtn.image.sprite = colorsList[index];
        // playerReveledColorbtn.image.sprite = colorsList[index+1];
        oneClrRevelbtn.image.sprite = colorsList[index + 1];

        TwoClrRevelbtn.image.sprite = colorsList[index];
        OponeTwoClrRevelbtn.image.sprite = colorsList[index];
        OPReveledColorbtn.image.sprite = colorsList[index];
        OponeClrRevelbtn.image.sprite = colorsList[index + 1];
        Debug.Log("Random TwoClrRevelbtns : " + OponeClrRevelbtn.image.sprite.name);
        switch (OponeTwoClrRevelbtn.image.sprite.name)
        {
            case "Silver":
                TwoColorReveled = Color.Silver;
                Debug.Log("Silver");
                break;
            case "Red":
                TwoColorReveled = Color.Red;
                Debug.Log("Red");
                break;
            case "Purple":
                TwoColorReveled = Color.Purple;
                Debug.Log("Purple");
                break;
            case "Green":
                TwoColorReveled = Color.Green;
                Debug.Log("Green");
                break;
            case "Blue":
                TwoColorReveled = Color.Blue;
                Debug.Log("Blue");
                break;
            case "Yellow":
                TwoColorReveled = Color.Yellow;
                Debug.Log("Yellow");
                break;
            case "Black":
                TwoColorReveled = Color.Black;
                Debug.Log("Black");
                break;
        }


        switch (OponeClrRevelbtn.image.sprite.name)
        {
            case "Silver":
                oneColorReveled = Color.Silver;
                Debug.Log("Silver");
                break;
            case "Red":
                oneColorReveled = Color.Red;
                Debug.Log("Red");
                break;
            case "Purple":
                oneColorReveled = Color.Purple;
                Debug.Log("Purple");
                break;
            case "Green":
                oneColorReveled = Color.Green;
                Debug.Log("Green");
                break;
            case "Blue":
                oneColorReveled = Color.Blue;
                Debug.Log("Blue");
                break;
            case "Yellow":
                oneColorReveled = Color.Yellow;
                Debug.Log("Yellow");
                break;
            case "Black":
                oneColorReveled = Color.Black;
                Debug.Log("Black");
                break;
        }
        Debug.Log("TwoClrRevelbtn: " + OPReveledColorbtn.image.sprite.name);
    }

    public void GameState()
    {
        photonView.RPC("CheckHand", PhotonTargets.AllViaServer);
    }


    public void CheckHandSCore()
    {
        switch (CurrentGameState)
        {
            case GameStates.GameInit:
                colorPanel.gameObject.SetActive(false);
                if (CardHandler.Instance.isHandReady && CardIndex != 4)
                {
                    StartCoroutine(HighlightSprite(CardIndex));
                }
                if (CardIndex == 4)
                {
                    CurrentGameState = GameStates.GameStart;
                }


                currentGameStateSync();

                break;
            case GameStates.GameStart:
                if (CardHandler.Instance.playerCardsList != null && Index != 6)
                {
                    CurrentGameState = GameStates.GameColor;
                }
                currentGameStateSync();

                break;
            case GameStates.GameColor:
                StartTurn();

                StartCoroutine(ColorMatching());

                currentGameStateSync();

                CurrentGameState = GameStates.GameOver;
                break;
            case GameStates.GameOver:
                MatchingColorsPoints();
                break;
        }
    }


    [PunRPC]
    public void CheckHand()
    {
        switch (CurrentGameState)
        {
            case GameStates.GameInit:
                colorPanel.gameObject.SetActive(false);
                if (CardHandler.Instance.isHandReady && CardIndex != 4)
                {
                    StartCoroutine(HighlightSprite(CardIndex));
                }
                //  if (CardIndex == 4)
                // {
                CurrentGameState = GameStates.GameStart;
                //  }
                currentGameStateSync();

                break;
            case GameStates.GameStart:
                if (CardHandler.Instance.playerCardsList != null && Index != 6)
                {
                    CurrentGameState = GameStates.GameColor;
                }
                currentGameStateSync();

                break;
            case GameStates.GameColor:
                StartTurn();

                StartCoroutine(ColorMatching());

                currentGameStateSync();

                CurrentGameState = GameStates.GameOver;
                break;
            case GameStates.GameOver:
                MatchingColorsPoints();
                break;
        }
    }

    public void MatchingColorsPoints()
    {
        for (var i = 0; i < 7; i++)
        {
            switch (localSelection)
            {
                case Color.Black:
                    if (CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>().color ==
                        CardHandler.CardColor.Black)
                    {
                        StartCoroutine(
                            CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>()
                                .HighlightCard());
                        ScoreValue +=
                            CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>().power;
                        scoreText.text = scoreValue.ToString();
                        switch (CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>().type1)
                        {
                            case CardHandler.Type1.Animal:

                                break;
                        }
                    }
                    if (CardHandler.Instance.OpponentCardsPlaceHolderList[i].GetComponentInChildren<Card>().color ==
                        CardHandler.CardColor.Black)
                    {
                        StartCoroutine(
                            CardHandler.Instance.OpponentCardsPlaceHolderList[i].GetComponentInChildren<Card>()
                                .HighlightCard());
                    }
                    break;
                case Color.Red:
                    if (CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>().color ==
                        CardHandler.CardColor.Red)
                    {
                        StartCoroutine(
                            CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>()
                                .HighlightCard());
                        ScoreValue +=
                            CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>().power;
                        scoreText.text = scoreValue.ToString();
                    }
                    break;
                case Color.Green:
                    if (CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>().color ==
                        CardHandler.CardColor.Green)
                    {
                        StartCoroutine(
                            CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>()
                                .HighlightCard());
                        ScoreValue +=
                            CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>().power;
                        scoreText.text = scoreValue.ToString();
                    }
                    break;
                case Color.Blue:
                    if (CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>().color ==
                        CardHandler.CardColor.Blue)
                    {
                        StartCoroutine(
                            CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>()
                                .HighlightCard());
                        ScoreValue +=
                            CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>().power;
                        scoreText.text = scoreValue.ToString();
                    }
                    break;
                case Color.Yellow:
                    if (CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>().color ==
                        CardHandler.CardColor.Yellow)
                    {
                        StartCoroutine(
                            CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>()
                                .HighlightCard());
                        ScoreValue +=
                            CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>().power;
                        scoreText.text = scoreValue.ToString();
                    }
                    break;
                case Color.Silver:
                    if (CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>().color ==
                        CardHandler.CardColor.Silver)
                    {
                        StartCoroutine(
                            CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>()
                                .HighlightCard());
                        ScoreValue +=
                            CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>().power;
                        scoreText.text = scoreValue.ToString();
                    }
                    break;
                case Color.Purple:
                    if (CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>().color ==
                        CardHandler.CardColor.Purple)
                    {
                        StartCoroutine(
                            CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>()
                                .HighlightCard());
                        ScoreValue +=
                            CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>().power;
                        scoreText.text = scoreValue.ToString();
                    }
                    break;
            }
        }

        #region

        //foreach (var playerCard in  CardHandler.Instance.playerCardsPlaceHolderList)
        //{


        //                if (playerCard.GetComponentInChildren<Card>().color == CardHandler.CardColor.Black)
        //                {
        //                    Debug.Log("Color.Black");
        //                    StartCoroutine(playerCard.GetComponentInChildren<Card>().HighlightCard());
        //                }


        //                if (playerCard.GetComponentInChildren<Card>().color == CardHandler.CardColor.Red)
        //                {
        //                    Debug.Log("Color.Red");
        //                    StartCoroutine(playerCard.GetComponentInChildren<Card>().HighlightCard());
        //                }


        //                if (playerCard.GetComponentInChildren<Card>().color == CardHandler.CardColor.Green)
        //                {
        //                    Debug.Log("Color.Green");
        //                    StartCoroutine(playerCard.GetComponentInChildren<Card>().HighlightCard());
        //                }


        //                if (playerCard.GetComponentInChildren<Card>().color == CardHandler.CardColor.Blue)
        //                {
        //                    Debug.Log("Color.Blue");
        //                    StartCoroutine(playerCard.GetComponentInChildren<Card>().HighlightCard());
        //                }


        //                if (playerCard.GetComponentInChildren<Card>().color == CardHandler.CardColor.Yellow)
        //                {
        //                    Debug.Log("Color.Yellow");
        //                    StartCoroutine(playerCard.GetComponentInChildren<Card>().HighlightCard());
        //                }


        //                if (playerCard.GetComponentInChildren<Card>().color == CardHandler.CardColor.Silver)
        //                {
        //                    Debug.Log("Color.Silver");
        //                    StartCoroutine(playerCard.GetComponentInChildren<Card>().HighlightCard());
        //                }


        //    }

        #endregion
    }


    public void currentGameStateSync()
    {
        photonView.RPC("GameStateSync", PhotonTargets.OthersBuffered, CurrentGameState);
    }


    [PunRPC]
    private void GameStateSync(GameStates currentState)
    {
        CurrentGameState = currentState;
    }

    public IEnumerator ColorMatching()
    {
        switch (oneColorReveled)
        {
            case Color.Black:

                for (var i = 0; i < 4; i++)
                {
                    if (
                        CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>().GetComponent<Card>().color ==
                        CardHandler.CardColor.Black)
                    {
                        countBlack++;
                        Debug.Log("Count " + countBlack);
                        scoreValue +=
                            CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>()
                                .GetComponent<Card>()
                                .power;
                        scoreText.text = scoreValue.ToString();
                        if (isOneColorReveled || isTwoColorReveled)
                        {
                            plCardCountTxt.text = countBlack.ToString();
                        }
                    }
                }
                break;
            case Color.Green:

                for (var i = 0; i < 4; i++)
                {
                    if (
                        CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>().GetComponent<Card>().color ==
                        CardHandler.CardColor.Green)
                    {
                        countgreen++;
                        Debug.Log("Count " + countgreen);
                        scoreValue +=
                            CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>()
                                .GetComponent<Card>()
                                .power;
                        scoreText.text = scoreValue.ToString();
                        if (isOneColorReveled || isTwoColorReveled)
                        {
                            plCardCountTxt.text = countgreen.ToString();
                        }
                    }
                }
                break;
            case Color.Red:

                for (var i = 0; i < 4; i++)
                {
                    if (
                        CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>().GetComponent<Card>().color ==
                        CardHandler.CardColor.Red)
                    {
                        countRed++;
                        Debug.Log("Count " + countRed);
                        scoreValue +=
                            CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>()
                                .GetComponent<Card>()
                                .power;
                        scoreText.text = scoreValue.ToString();
                        if (isOneColorReveled || isTwoColorReveled)
                        {
                            plCardCountTxt.text = countRed.ToString();
                        }
                    }
                }
                break;
            case Color.Yellow:

                for (var i = 0; i < 4; i++)
                {
                    if (
                        CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>().GetComponent<Card>().color ==
                        CardHandler.CardColor.Yellow)
                    {
                        countYellow++;
                        Debug.Log("Count " + countYellow);
                        scoreValue +=
                            CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>()
                                .GetComponent<Card>()
                                .power;
                        scoreText.text = scoreValue.ToString();
                        if (isOneColorReveled || isTwoColorReveled)
                        {
                            plCardCountTxt.text = countYellow.ToString();
                        }
                    }
                }
                break;
            case Color.Orange:

                for (var i = 0; i < 4; i++)
                {
                    if (
                        CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>().GetComponent<Card>().color ==
                        CardHandler.CardColor.Orange)
                    {
                        countOrange++;
                        Debug.Log("Count " + countOrange);
                        scoreValue +=
                            CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>()
                                .GetComponent<Card>()
                                .power;
                        scoreText.text = scoreValue.ToString();
                        if (isOneColorReveled || isTwoColorReveled)
                        {
                            plCardCountTxt.text = countOrange.ToString();
                        }
                    }
                }
                break;
            case Color.Silver:

                for (var i = 0; i < 4; i++)
                {
                    if (
                        CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>().GetComponent<Card>().color ==
                        CardHandler.CardColor.Silver)
                    {
                        countSilver++;
                        Debug.Log("Count " + countSilver);
                        scoreValue +=
                            CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>()
                                .GetComponent<Card>()
                                .power;
                        scoreText.text = scoreValue.ToString();
                        if (isOneColorReveled || isTwoColorReveled)
                        {
                            plCardCountTxt.text = countSilver.ToString();
                        }
                    }
                }
                break;
            case Color.Purple:
                var countPurple = 0;
                for (var i = 0; i < 4; i++)
                {
                    if (
                        CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>().GetComponent<Card>().color ==
                        CardHandler.CardColor.Purple)
                    {
                        countPurple++;
                        Debug.Log("Count " + countPurple);
                        scoreValue +=
                            CardHandler.Instance.playerCardsList[i].GetComponent<CardNetwork>()
                                .GetComponent<Card>()
                                .power;
                        scoreText.text = scoreValue.ToString();
                        if (isOneColorReveled || isTwoColorReveled)
                        {
                            plCardCountTxt.text = countPurple.ToString();
                        }
                    }
                }
                break;
        }

        yield return new WaitForSeconds(1.5f);
    }


    public IEnumerator HighlightSprite(int index)
    {
        yield return null;
        //StartCoroutine(CardHandler.Instance.playerCardsPlaceHolderList[index].GetComponentInChildren<Card>().Flip(defaultSprite));
        //StartCoroutine(CardHandler.Instance.OpponentCardsPlaceHolderList[index].GetComponentInChildren<Card>().Flip(defaultSprite));
        for (var i = 0; i < 4; i++)
        {
            yield return new WaitForSeconds(1.5f);
            StartCoroutine(
                CardHandler.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<Card>().HighlightCard());
            StartCoroutine(
                CardHandler.Instance.OpponentCardsPlaceHolderList[i].GetComponentInChildren<Card>().HighlightCard());
            ScoreValue +=
                CardHandler.Instance.playerCardsPlaceHolderList[i].transform.GetChild(0).GetComponent<Card>().value;
            ScoreValueOp += CardHandler.Instance.OpponentCardsList[i].GetComponent<Card>().value;
            opScoreText.text = ScoreValueOp.ToString();
        }


        //StartCoroutine(
        //    CardHandler.Instance.playerCardsPlaceHolderList[index].GetComponentInChildren<Card>().HighlightCard());
        //StartCoroutine(
        //    CardHandler.Instance.OpponentCardsPlaceHolderList[index].GetComponentInChildren<Card>().HighlightCard());
        //CardIndex++;

        //   CardHandler.Instance.playerCardsList[0].GetComponent<Card>().bonusType =
        Debug.Log("HIghlighted sprite with score update");
        CurrentGameState = GameStates.GameStart;

        //  Index++;
        //CheckHand();
        //ScoreValue +=
        //    CardHandler.Instance.playerCardsPlaceHolderList[index].transform.GetChild(0).GetComponent<Card>().value;
        //ScoreValueOp += CardHandler.Instance.OpponentCardsList[index].GetComponent<Card>().value;
        //opScoreText.text = ScoreValueOp.ToString();
        //GameState();
    }

    public IEnumerator OpenSprite(int index)
    {
        yield return new WaitForSeconds(1.5f);
        StartCoroutine(
            CardHandler.Instance.playerCardsPlaceHolderList[index].GetComponentInChildren<Card>()
                .Flip(CardHandler.Instance.playerCardsList[index].GetComponent<Card>().sprite));
        StartCoroutine(
            CardHandler.Instance.OpponentCardsPlaceHolderList[index].GetComponentInChildren<Card>()
                .Flip(CardHandler.Instance.OpponentCardsList[index].GetComponent<Card>().sprite));
        //   CardHandler.Instance.playerCardsList[0].GetComponent<Card>().bonusType =

        Index++;
        CheckHand();
        ScoreValue += CardHandler.Instance.playerCardsList[index].GetComponent<Card>().value;
        ScoreValueOp += CardHandler.Instance.OpponentCardsList[index].GetComponent<Card>().value;
        opScoreText.text = ScoreValueOp.ToString();
    }


    public void ReadyCards()
    {
        for (var i = 0; i < 4; i++)
        {
            photonView.RPC("othercardsSet", PhotonTargets.OthersBuffered,
                CardHandler.Instance.playerCardsPlaceHolderList[i].transform.GetChild(0).GetComponent<Card>().id, i);

            Debug.Log(CardHandler.Instance.playerCardsPlaceHolderList[i].transform.GetChild(0).GetComponent<Card>().id +
                      i + "CardHandler.Instance.playerCardsPlaceHolderList[i].transform.parent.gameObject.name");
        }
        CardHandler.Instance.initializeSecondRoundCard();
    }

    public void SecondRoundReadyCards()
    {
        for (var i = 4; i < 7; i++)
        {
            photonView.RPC("othersecondRoundCardSet", PhotonTargets.OthersBuffered,
                CardHandler.Instance.playerCardsPlaceHolderList[i].transform.GetChild(0).GetComponent<Card>().id, i);
        }
    }


    [PunRPC]
    public void othersecondRoundCardSet(int id, int pos)
    {
        CardHandler.Instance.isHandReady = true;
        for (var i = 4; i < 7; i++)
        {
            CardHandler.Instance.OpponentCardsList[i].transform.SetParent( CardHandler.Instance.OpponentCardsPlaceHolderList[i].transform);
        }


        for (var i = 4; i < 7; i++)
        {
            foreach (var cards in CardHandler.Instance.cards)
            {
                if (cards.id == id)
                {
                    CardHandler.Instance.OpponentCardsList[pos].GetComponent<CardNetwork>().GetComponent<Card>().id =
                        cards.id;
                    CardHandler.Instance.OpponentCardsList[pos].GetComponent<CardNetwork>().GetComponent<Image>().sprite
                        = cards.sprite;
                    CardHandler.Instance.OpponentCardsList[pos].GetComponent<CardNetwork>().GetComponent<Card>().sprite
                        =
                        cards.sprite;
                    CardHandler.Instance.OpponentCardsList[pos].GetComponent<CardNetwork>().GetComponent<Card>().value =
                        cards.value;
                    CardHandler.Instance.OpponentCardsList[pos].GetComponent<CardNetwork>().GetComponent<Card>().power =
                        cards.power;
                }
            }
        }
    }


    [PunRPC]
    public void othercardsSet(int id, int pos)
    {
        // CurrentGameState = GameStates.GameColor;
        CardHandler.Instance.isHandReady = true;
        for (var i = 0; i < 4; i++)
        {
            CardHandler.Instance.OpponentCardsList[i].transform.SetParent(
                CardHandler.Instance.OpponentCardsPlaceHolderList[i].transform);
        }


        for (var i = 0; i < 4; i++)
        {
            foreach (var cards in CardHandler.Instance.cards)
            {
                if (cards.id == id)
                {
                   
                    CardHandler.Instance.OpponentCardsList[pos].GetComponent<CardNetwork>().GetComponent<Card>().id =
                        cards.id;
                    CardHandler.Instance.OpponentCardsList[pos].GetComponent<CardNetwork>().GetComponent<Image>().sprite
                        = cards.sprite;
                    CardHandler.Instance.OpponentCardsList[pos].GetComponent<CardNetwork>().GetComponent<Card>().sprite
                        =
                        cards.sprite;
                    CardHandler.Instance.OpponentCardsList[pos].GetComponent<CardNetwork>().GetComponent<Card>().value =
                        cards.value;
                    CardHandler.Instance.OpponentCardsList[pos].GetComponent<CardNetwork>().GetComponent<Card>().power =
                        cards.power;
                    Destroy(CardHandler.Instance.OpponentCardsList[pos].GetComponent<Draggable>());
                }
            }
        }


        Debug.Log(id + ">>>>>>>>>>>>>>>" + name);
    }


    [PunRPC]
    public void DrawMyCardsOnOthers(int data, int viewID, int ownerId)
    {
        Debug.Log(data + "|Location_pos----|" + "ViewID" + viewID + "Owner ID" + ownerId);


        foreach (var OpCard in CardHandler.Instance.OpponentCardsList)
        {
            if (OpCard.GetPhotonView().viewID == viewID)
            {
                Debug.Log("OpCard.GetPhotonView().viewID =" + OpCard.GetPhotonView().viewID + "RPC viewID->" +
                          viewID);
                //   OpCard.GetComponent<CardNetwork>().GetComponent<Image>().sprite =CardHandler.Instance.playerCardsList[data].GetComponent<Card>().sprite;
                OpCard.transform.SetParent(CardHandler.Instance.OpponentCardsPlaceHolderList[data].transform);
                OpCard.GetComponent<CardNetwork>().GetComponent<Image>().sprite =
                    CardHandler.Instance.playerCardsList[data].GetComponent<Image>().sprite;
                OpCard.GetComponent<Card>().value = CardHandler.Instance.cards[data].value;
                OpCard.GetComponent<Card>().power = CardHandler.Instance.cards[data].power;
            }
        }
    }


    private void Update()
    {
        UpdateTimeText();
    }

    public void broadCast(int i)
    {
        Debug.Log("OnTurnBegins() turn: " + i);
    }

    public void OnTurnBegins(int turn)
    {
        Debug.Log("OnTurnBegins() turn: " + turn);
        //this.localSelection = Hand.None;
        //this.remoteSelection = Hand.None;

        //this.WinOrLossImage.gameObject.SetActive(false);

        //this.localSelectionImage.gameObject.SetActive(false);
        //this.remoteSelectionImage.gameObject.SetActive(true);

        //IsShowingResults = false;
        //ButtonCanvasGroup.interactable = true;
        colorPanel.gameObject.SetActive(true);
        UpdateTimeText();
    }

    private void UpdateTimeText()
    {
        timerText.text = turnManager.RemainingSecondsInTurn.ToString("F1") + "Seconds";
    }

    public void OnClickBlack()
    {
        MakeTurn(Color.Black);
        localSelection = Color.Black;
        colorSelectBtn.GetComponent<Image>().color = UnityEngine.Color.black;
        colorPanel.gameObject.SetActive(false);
    }

    public void OnClickBlue()
    {
        MakeTurn(Color.Blue);
        localSelection = Color.Blue;
        colorSelectBtn.GetComponent<Image>().color = UnityEngine.Color.blue;
        colorPanel.gameObject.SetActive(false);
    }

    public void OnClickYellow()
    {
        MakeTurn(Color.Yellow);
        localSelection = Color.Yellow;
        colorSelectBtn.GetComponent<Image>().color = UnityEngine.Color.yellow;
        colorPanel.gameObject.SetActive(false);
    }

    public void OnClickGreen()
    {
        MakeTurn(Color.Green);
        localSelection = Color.Green;
        colorSelectBtn.GetComponent<Image>().color = UnityEngine.Color.green;
        colorPanel.gameObject.SetActive(false);
    }

    public void OnClickOrange()
    {
        MakeTurn(Color.Orange);
        localSelection = Color.Orange;
        colorSelectBtn.GetComponent<Image>().sprite = Orange;
        colorPanel.gameObject.SetActive(false);
    }

    public void OnClickSilver()
    {
        MakeTurn(Color.Silver);
        localSelection = Color.Silver;
        colorSelectBtn.GetComponent<Image>().sprite = Silver;
        colorPanel.gameObject.SetActive(false);
    }

    public void OnClickRed()
    {
        MakeTurn(Color.Red);
        localSelection = Color.Red;
        colorSelectBtn.GetComponent<Image>().color = UnityEngine.Color.red;
        colorPanel.gameObject.SetActive(false);
    }

    public void OnTurnCompleted(int turn)
    {
        OnEndTurn();
    }

    public void OnPlayerMove(PhotonPlayer player, int turn, object move)
    {
        Debug.Log("OnPlayerMove: " + player + " turn: " + turn + " action: " + move);
        //throw new NotImplementedException();
    }

    public void OnPlayerFinished(PhotonPlayer player, int turn, object move)
    {
        Debug.Log("OnTurnFinished: " + player + " turn: " + turn + " action: " + move);


        if (player.IsLocal)
        {
            localSelection = (Color) (byte) move;
        }
        else
        {
            remoteSelection = (Color) (byte) move;
        }
    }

    public void OnTurnTimeEnds(int turn)
    {
        OnTurnCompleted(-1);
    }

    public void StartTurn()
    {
        if (PhotonNetwork.isMasterClient)
        {
            turnManager.BeginTurn();
        }
    }

    public void MakeTurn(Color selection)
    {
        turnManager.SendMove((byte) selection, true);
    }

    public void OnEndTurn()
    {
        //    this.StartCoroutine("ShowResultsBeginNextTurnCoroutine");
        colorPanel.gameObject.SetActive(false);
    }


    public void setCardDetails(Sprite imageSprite, Color color, CardHandler.Type1 type1, CardHandler.Type2 type2)
    {
        CardDetails.sprite = imageSprite;
    }

   
    public void CardLoaded(int index, bool isloaded)
    {
        if (index == 1)
        {
            cardOneIsLoaded = isloaded;
        }
        if (index == 2)
        {
            cardTwoIsLoaded = isloaded;
        }
        if (index == 3)
        {
            cardThreeIsLoaded = isloaded;
        }
        if (index == 4)
        {
            cardFourIsLoaded = isloaded;
        }
        if (index == 5)
        {
            cardFiveIsLoaded = isloaded;
        }
        if (index == 6)
        {
            cardSixIsLoaded= isloaded;
        }
        if (index == 7)
        {
            cardSevenIsLoaded = isloaded;
        }
    }

    public interface IGameHandler
    {
        void setCardDetails(Sprite imageSprite);
        void CardLoaded(int index, bool isloaded);
        void ReadyCards();
        void GameState();
        void CheckHandSCore();
        void SecondRoundReadyCards();
    }
}

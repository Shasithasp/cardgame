﻿using System;
using UnityEngine;
using System.Collections;
using Photon;
 
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ConnectPlayer : PunBehaviour
{
    // Use this for initialization
    public InputField inputField;
    private bool connectFailed = false;

    public static readonly string SceneNameMenu = "Connecting";

    public static readonly string SceneNameGame = "Main Menu";


    void Awake()
    {
        // make sure every player is always on the same scene
        PhotonNetwork.automaticallySyncScene = true;
    }

    private void Start()
    {
        Connect();
        PhotonNetwork.ConnectUsingSettings("0.1");
    }

    // Update is called once per frame
    private void Update()
    {

    }

    public void OnGUI()
    {
        //if (this.Skin != null)
        //{
        //    GUI.skin = this.Skin;
        //}

        if (!PhotonNetwork.connected)
        {
            if (PhotonNetwork.connecting)
            {
                GUILayout.Label("Connecting to: " + PhotonNetwork.ServerAddress);
            }
            else
            {
                GUILayout.Label("Not connected. Check console output. Detailed connection state: " +
                                PhotonNetwork.connectionStateDetailed + " Server: " + PhotonNetwork.ServerAddress);
            }

            if (this.connectFailed)
            {
                GUILayout.Label("Connection failed. Check setup and use Setup Wizard to fix configuration.");
                GUILayout.Label(String.Format("Server: {0}", new object[] { PhotonNetwork.ServerAddress }));
                GUILayout.Label("AppId: " + PhotonNetwork.PhotonServerSettings.AppID.Substring(0, 8) + "****");
                // only show/log first 8 characters. never log the full AppId.

                if (GUILayout.Button("Try Again", GUILayout.Width(100)))
                {
                    this.connectFailed = false;
                    PhotonNetwork.ConnectUsingSettings("0.9");
                }
            }

            return;
        }
    }

    //void OnGUI()
    //{
    //    // join a random room
    //    if (GUILayout.Button("Join Random", GUILayout.Width(200f)))
    //    {
    //        PhotonNetwork.JoinRandomRoom();
    //    }

    //    // create a new room
    //    if (GUILayout.Button("Create Room", GUILayout.Width(200f)))
    //    {
    //        PhotonNetwork.CreateRoom("Myroom");
    //    }

    //    // show the friends list management page
    //    if (GUILayout.Button("Friends", GUILayout.Width(200f)))
    //    {
    //        gameObject.SetActive(false);
    //        //  FriendsListScreen.SetActive(true);
    //    }

    //    RoomInfo[] rooms = PhotonNetwork.GetRoomList();

    //    // no rooms available, inform the user
    //    if (rooms.Length == 0)
    //    {
    //        GUILayout.Label("No Rooms Available");
    //    }
    //    else
    //    {
    //        // show a scrollable list of rooms

    //        //  lobbyScroll = GUILayout.BeginScrollView(lobbyScroll, GUILayout.Width(220f), GUILayout.ExpandHeight(true));

    //        foreach (RoomInfo room in PhotonNetwork.GetRoomList())
    //        {
    //            GUILayout.BeginHorizontal(GUILayout.Width(200f));

    //            // display room name and capacity
    //            GUILayout.Label(room.name + " - " + room.playerCount + "/" + room.maxPlayers);

    //            // connect to the room
    //            if (GUILayout.Button("Enter"))
    //            {
    //                PhotonNetwork.JoinRoom(room.name);
    //            }

    //            GUILayout.EndHorizontal();
    //        }

    //        GUILayout.EndScrollView();
    //    }
    //}
    public void CreateRoomBtnClicked()
    {


        
            PhotonNetwork.CreateRoom(inputField.text, new RoomOptions() {maxPlayers = 2}, null);
       
        //var roomName = "sdasd";
        //if (PhotonNetwork.isMasterClient)
        //{

        //    PhotonNetwork.CreateRoom(roomName);
        //    SceneManager.LoadScene("Main Menu");
        //    Debug.Log("Room Created");
        //}
        //else
        //{
        //    Debug.Log("Room NotCreated");
        //    PhotonNetwork.CreateRoom(roomName);
        //}
    }

     
    public void OnConnectedToMaster()
    {
        // this method gets called by PUN, if "Auto Join Lobby" is off.
        // this demo needs to join the lobby, to show available rooms!
        Debug.Log("OnConnectedTOMaster");
        PhotonNetwork.JoinLobby();


    }
    public void OnPhotonPlayerConnected(PhotonPlayer other)
    {
        Debug.Log("OnPhotonPlayerConnected() " + other.NickName); // not seen if you're the player connecting

        if (PhotonNetwork.isMasterClient)
        {
            Debug.Log("OnPhotonPlayerConnected isMasterClient " + PhotonNetwork.isMasterClient); // called before OnPhotonPlayerDisconnected

            
        }
    }
public void OnCreatedRoom()
    {
        Debug.Log("OnCreatedRoom");
    Debug.Log(PhotonNetwork.room.playerCount);
         PhotonNetwork.LoadLevel(SceneNameGame);
    }
    public void Connect()
    {
        // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
        PhotonNetwork.automaticallySyncScene = true;

        // the following line checks if this client was just created (and not yet online). if so, we connect
        //if (PhotonNetwork.connectionStateDetailed == PeerState.PeerCreated)
        //{
        //    // Connect to the photon master-server. We use the settings saved in PhotonServerSettings (a .asset file in this project)
        //    PhotonNetwork.ConnectUsingSettings("0.9");
        //}

        // generate a name for this player, if none is assigned yet
        if (String.IsNullOrEmpty(PhotonNetwork.playerName))
        {
          //  PhotonNetwork.playerName = GUIHandler.Instance.userName;
        }

        // if you wanted more debug out, turn this on:
        // PhotonNetwork.logLevel = NetworkLogLevel.Full;

        //StartCoroutine(RefreshData());
        //UserName.text = "Player Name : " + PhotonNetwork.playerName;
        //StartCoroutine(RefreshRooms());
    }
    public void OnMasterClientSwitched(PhotonPlayer player)
    {
        Debug.Log("OnMasterClientSwitched: " + player);

       
    }

    public void LoadScene()
    {
        if ( PhotonNetwork.connectedAndReady)
        {
            Application.LoadLevel(1);
        }
    }
}

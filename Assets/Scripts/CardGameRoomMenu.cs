﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CardGameRoomMenu :  MonoBehaviour 
{


//	public GUISkin Skin;
//    public Vector2 WidthAndHeight = new Vector2(600, 400);
//    private string roomName = "myRoom";
//
//    private Vector2 scrollPos = Vector2.zero;

    private bool connectFailed = false;

    public static readonly string SceneNameMenu = "Main";

    public static readonly string SceneNameGame = "Main Menu";

    private string errorDialog;
    private double timeToClearDialog;

    [Header("UI Elements")] public Text UserName;
    public GameObject RoomParent;
    public GameObject RoomPrefabUI;
    public InputField RoomNameInputField;
    
    public Text pendingStatusText;
    public Text ErrorText;
    
    
 
    public void Start()
    {
       // ErrorPanel.SetActive(false);
        PhotonNetwork.autoJoinLobby = true;
    }

    public void Connect()
    {
        // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
        PhotonNetwork.automaticallySyncScene = true;

        // the following line checks if this client was just created (and not yet online). if so, we connect
        if (PhotonNetwork.connectionStateDetailed == ClientState.PeerCreated)
        {
            // Connect to the photon master-server. We use the settings saved in PhotonServerSettings (a .asset file in this project)
            PhotonNetwork.ConnectUsingSettings("v1.0");
        }

        // generate a name for this player, if none is assigned yet
        if (String.IsNullOrEmpty(PhotonNetwork.playerName))
        {
            //PhotonNetwork.playerName = GUIHandler.Instance.userName;
        }

        // if you wanted more debug out, turn this on:
        // PhotonNetwork.logLevel = NetworkLogLevel.Full;

        StartCoroutine(RefreshData());
   //   UserName.text = "Player Name : " + PhotonNetwork.playerName;
        StartCoroutine(RefreshRooms());
    }

    private IEnumerator RefreshData()
    {
        while (!PhotonNetwork.connected)
        {
           GUIHandler.Instance.statsText.text = "Not connected. Check console output. Detailed connection state: " + PhotonNetwork.connectionStateDetailed + " Server: " + PhotonNetwork.ServerAddress;
            pendingStatusText.text = "please wait....!!";
            yield return new WaitForSeconds(0.5f);
        }

        while (PhotonNetwork.connecting)
        {
            pendingStatusText.text = "Connecting to: " + PhotonNetwork.ServerAddress;
            yield return new WaitForSeconds(0.5f);
        }

        while (PhotonNetwork.connected)
        {
            GUIHandler.Instance.statsText.text = PhotonNetwork.countOfPlayers + " users are online in " + PhotonNetwork.countOfRooms + " rooms.";
       pendingStatusText.text = PhotonNetwork.countOfPlayers + " users are online in " + PhotonNetwork.countOfRooms + " rooms."; ;
            yield return new WaitForSeconds(0.5f);
        }
    }

    private IEnumerator RefreshRooms()
    {
        while (!PhotonNetwork.connected)
        {
            yield return null;
        }

        while (PhotonNetwork.connecting)
        {
            yield return null;
        }
        while (PhotonNetwork.connected)
        {
            CreateRoomList();
           //Debug.Log ("Rooms Refreshed");
            yield return new WaitForSeconds(2f);
        }
    }

    private void CreateRoomList()
    {
        //Debug.Log(PhotonNetwork.insideLobby);
        for (var i = RoomParent.transform.childCount - 1; i >= 0; i--)
        {
            // objectA is not the attached GameObject, so you can do all your checks with it.
            var obj = RoomParent.transform.GetChild(i);
            Destroy(obj.gameObject);
            // Optionally destroy the objectA if not longer needed
        }
     
            //Debug.Log(PhotonNetwork.GetRoomList().Length.ToString());
        foreach (var item in PhotonNetwork.GetRoomList())
        {
            GameObject go = Instantiate(RoomPrefabUI) as GameObject;
            go.transform.SetParent(RoomParent.transform);
            go.transform.localScale = new Vector3(1, 1, 1);
            go.transform.GetChild(0).GetComponent<Text>().text = item.name;
            go.transform.GetChild(1).GetComponent<Text>().text = item.playerCount + "/" + item.maxPlayers;
            AttachOnClickEvent(go.transform.GetChild(2).gameObject.GetComponent<Button>(), item.name);
            Debug.Log("Create Room prefab");
        }
    }

    private void AttachOnClickEvent(Button button, string roomName)
    {
        button.onClick.AddListener(() => OnJoinRoomClicked(roomName));
    }

    public void OnJoinRoomClicked(string roomName)
    {
        PhotonNetwork.JoinRoom(roomName);





    }

    public void JoinRandomRoomButtonClicked()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    public void CreateRoomButtonClicked()
    {


        if (RoomNameInputField.text != "")
            PhotonNetwork.CreateRoom(RoomNameInputField.text, new RoomOptions() { maxPlayers = 2, PlayerTtl = 5000 },new TypedLobby(){Type = LobbyType.Default});



    }

    public void Update()
    {
        
        //if (!string.IsNullOrEmpty(ErrorDialog))
        //{
        //    ErrorPanel.SetActive(true);
        //    ErrorText.text = ErrorDialog;

        //    if (this.timeToClearDialog < Time.time)
        //    {
        //        this.timeToClearDialog = 0;
        //        ErrorDialog = "";
        //        ErrorPanel.SetActive(false);
        //    }
        //}
    }

//    public void OnGUI()
//    {
////        if (this.Skin != null)
////        {
////            GUI.skin = this.Skin;
////        }

//        if (!PhotonNetwork.connected)
//        {
//            if (PhotonNetwork.connecting)
//            {
//                GUILayout.Label("Connecting to: " + PhotonNetwork.ServerAddress);
//            }
//            else
//            {
//                GUILayout.Label("Not connected. Check console output. Detailed connection state: " + PhotonNetwork.connectionStateDetailed + " Server: " + PhotonNetwork.ServerAddress);
//            }

//            if (this.connectFailed)
//            {
//                GUILayout.Label("Connection failed. Check setup and use Setup Wizard to fix configuration.");
//                GUILayout.Label(String.Format("Server: {0}", new object[] {PhotonNetwork.ServerAddress}));
//                GUILayout.Label("AppId: " + PhotonNetwork.PhotonServerSettings.AppID.Substring(0, 8) + "****"); // only show/log first 8 characters. never log the full AppId.

//                if (GUILayout.Button("Try Again", GUILayout.Width(100)))
//                {
//                    this.connectFailed = false;
//                    PhotonNetwork.ConnectUsingSettings("0.9");
//                }
//            }

//            return;
//        }

////        Rect content = new Rect((Screen.width - this.WidthAndHeight.x)/2, (Screen.height - this.WidthAndHeight.y)/2, this.WidthAndHeight.x, this.WidthAndHeight.y);
////        GUI.Box(content, "Join or Create Room");
////        GUILayout.BeginArea(content);
////
////        GUILayout.Space(40);
////
////        // Player name
////        GUILayout.BeginHorizontal();
////        GUILayout.Label("Player name:", GUILayout.Width(150));
////        PhotonNetwork.playerName = GUILayout.TextField(PhotonNetwork.playerName);
////        GUILayout.Space(158);
////        if (GUI.changed)
////        {
////            // Save name
////            PlayerPrefs.SetString("playerName", PhotonNetwork.playerName);
////        }
////        GUILayout.EndHorizontal();
////
////        GUILayout.Space(15);
////
////        // Join room by title
////        GUILayout.BeginHorizontal();
////        GUILayout.Label("Roomname:", GUILayout.Width(150));
////        this.roomName = GUILayout.TextField(this.roomName);
////
////        if (GUILayout.Button("Create Room", GUILayout.Width(150)))
////        {
////            PhotonNetwork.CreateRoom(this.roomName, new RoomOptions() {maxPlayers = 10}, null);
////        }
////
////        GUILayout.EndHorizontal();
////
////        // Create a room (fails if exist!)
////        GUILayout.BeginHorizontal();
////        GUILayout.FlexibleSpace();
////        //this.roomName = GUILayout.TextField(this.roomName);
////        if (GUILayout.Button("Join Room", GUILayout.Width(150)))
////        {
////            PhotonNetwork.JoinRoom(this.roomName);
////        }
////
////        GUILayout.EndHorizontal();
////
////
////        if (!string.IsNullOrEmpty(ErrorDialog))
////        {
////            GUILayout.Label(ErrorDialog);
////
////            if (this.timeToClearDialog < Time.time)
////            {
////                this.timeToClearDialog = 0;
////                ErrorDialog = "";
////            }
////        }
////
////        GUILayout.Space(15);
////
////        // Join random room
////        GUILayout.BeginHorizontal();
////
////        GUILayout.Label(PhotonNetwork.countOfPlayers + " users are online in " + PhotonNetwork.countOfRooms + " rooms.");
////        GUILayout.FlexibleSpace();
////        if (GUILayout.Button("Join Random", GUILayout.Width(150)))
////        {
////            PhotonNetwork.JoinRandomRoom();
////        }
////
////
////        GUILayout.EndHorizontal();
////
////        GUILayout.Space(15);
////        if (PhotonNetwork.GetRoomList().Length == 0)
////        {
////            GUILayout.Label("Currently no games are available.");
////            GUILayout.Label("Rooms will be listed here, when they become available.");
////        }
////        else
////        {
////            GUILayout.Label(PhotonNetwork.GetRoomList().Length + " rooms available:");
////
////            // Room listing: simply call GetRoomList: no need to fetch/poll whatever!
////            this.scrollPos = GUILayout.BeginScrollView(this.scrollPos);
////            foreach (RoomInfo roomInfo in PhotonNetwork.GetRoomList())
////            {
////                GUILayout.BeginHorizontal();
////                GUILayout.Label(roomInfo.name + " " + roomInfo.playerCount + "/" + roomInfo.maxPlayers);
////                if (GUILayout.Button("Join", GUILayout.Width(150)))
////                {
////                    PhotonNetwork.JoinRoom(roomInfo.name);
////                }
////
////                GUILayout.EndHorizontal();
////            }
////
////            GUILayout.EndScrollView();
////        }
////
////        GUILayout.EndArea();
//    }

    // We have two options here: we either joined(by title, list or random) or created a room.
    public void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom");
    }

    public void OnPhotonCreateRoomFailed()
    {
       // ErrorDialog = "Error: Can't create room (room name maybe already used).";
        Debug.Log(
            "OnPhotonCreateRoomFailed got called. This can happen if the room exists (even if not visible). Try another room name.");
        PhotonNetwork.autoJoinLobby = true;
    }

    public void OnPhotonJoinRoomFailed(object[] cause)
    {
       // ErrorDialog = "Error: Can't join room (full or unknown room name). " + cause[1];
        Debug.Log("OnPhotonJoinRoomFailed got called. This can happen if the room is not existing or full or closed.");
        PhotonNetwork.CreateRoom(RoomNameInputField.text, new RoomOptions() { maxPlayers = 1 ,PlayerTtl = 5000}, null);
    }

    public void OnPhotonRandomJoinFailed()
    {
      //  ErrorDialog = "Error: Can't join random room (none found).";
        Debug.Log(
            "OnPhotonRandomJoinFailed got called. Happens if no room is available (or all full or invisible or closed). JoinrRandom filter-options can limit available rooms.");
        PhotonNetwork.autoJoinLobby = true;
        PhotonNetwork.JoinLobby();
    }

    public void OnCreatedRoom()
    {
        //Debug.Log("OnCreatedRoom");
        PhotonNetwork.LoadLevel(SceneNameGame);
    }

    public void OnDisconnectedFromPhoton()
    {
        Debug.Log("Disconnected from Photon.");

    }

    public void OnFailedToConnectToPhoton(object parameters)
    {
        this.connectFailed = true;
        Debug.Log("OnFailedToConnectToPhoton. StatusCode: " + parameters + " ServerAddress: " + PhotonNetwork.ServerAddress);
    }
}
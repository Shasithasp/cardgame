using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerNetwork : Photon.MonoBehaviour
{
//    ThirdPersonCamera cameraScript;
//    ThirdPersonController controllerScript;

    
void Awake()
    {
//        cameraScript = GetComponent<ThirdPersonCamera>();
//        controllerScript = GetComponent<ThirdPersonController>();

        if (photonView.isMine)
        {
            DontDestroyOnLoad(gameObject);
            //MINE: local player, simply enable the local scripts
          //  gameObject.GetComponent<Player>().PlayerAvatar = gameObject.GetComponent<Player>() .GetImage(photonView.ownerId-1);
          //  gameObject.GetComponent<Player>().displayNameText.text = gameObject.GetComponent<Player>().playerName;
            //Debug.Log (gameObject.GetComponent<Player> ().playerColor);
            //int ival = int.Parse(gameObject.GetComponent<Player>().PlayerAvatar.name);
            gameObject.GetComponent<Player>().name = "Other";
            //Sprite LocalAvatar = gameObject.GetComponent<Player>().PlayerAvatar;
            //  Debug.Log(ival);
            //int index = int.Parse(LocalAvatar.name);
            //photonView.RPC("ChangeAvatar", PhotonTargets.AllBuffered, index);

        }
        else
        {           
			PlayerHandler.Instance.AddPlayer(gameObject, photonView);

           
        }
			
    }
			
   
 

  

 
	public void ActivateOrDeactivateTurnFlagCall(bool isActivate)
	{
		photonView.RPC("ActivateOrDeactivateTurnFlag", PhotonTargets.All, isActivate);
	}

	[PunRPC]
	void ActivateOrDeactivateTurnFlag(bool isActivate)
	{
		gameObject.GetComponent<Player> ().turnFlag.gameObject.SetActive(isActivate);
		gameObject.GetComponent<Player> ().StartTurnTimer ();
        //Debug.Log("Flag Activate " + isActivate);
	}

   


    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            //We own this player: send the others our data
            //stream.SendNext(gameObject.GetComponent<Card>().GetComponent<GameObject>().GetComponentsInParent<Image>());
//			Color color = gameObject.GetComponent<Player>().playerColor;
//			stream.SendNext(new Vector3(color.r,color.g,color.b));
       stream.SendNext(transform.position);
//            stream.SendNext(transform.rotation); 
            //int money = gameObject.GetComponent<Player>().Money;
           // stream.SendNext(money);
        }
        else
        {
            //Network player, receive data
		Vector3 v3 = (Vector3)stream.ReceiveNext();
		//gameObject.GetComponent<Player> ().playerColor = new Color (v3.x, v3.y, v3.z);
//            correctPlayerPos = (Vector3)stream.ReceiveNext();
//            correctPlayerRot = (Quaternion)stream.ReceiveNext();
        }
    }

    void Update()
    {
       
    }
    

}
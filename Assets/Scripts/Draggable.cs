﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class Draggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler 
{
    public Transform parentToReturnto;

    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("OnBeginDrag draggable  " + gameObject.transform.parent.tag);
        parentToReturnto = transform.parent;
        transform.SetParent(transform.parent.parent);
        GetComponent<CanvasGroup>().blocksRaycasts = false;
 
    }

    public void OnDrag(PointerEventData eventData)
    {
        
        transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        //Debug.Log("OnEndDrag"+"===="+this.gameObject.transform.parent.GetChild(0).gameObject.name+"-selectedobbj-"+eventData.pointerDrag);
        transform.SetParent(parentToReturnto);
        //  this.gameObject.transform.GetChild(0).GetComponent<CardNetwork>().ChangeTargetCall(CardHandler.Instance.playerCardsList[]);
        //  this.gameObject.transform.parent.GetChild(0).GetComponent<CardNetwork>().ChangeTargetCall(this.gameObject.transform.parent.GetChild(0).gameObject.name);
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        Debug.Log("This is the end place " + transform.parent.name);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("The cursor entered the selectable UI element.: " +
                  eventData.selectedObject.GetComponent<Card>().value);
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Card")
            //  coll.gameObject.SendMessage("ApplyDamage", 10);
            Debug.Log("OnCollisionEnter2D");
    }


   

    
 
}

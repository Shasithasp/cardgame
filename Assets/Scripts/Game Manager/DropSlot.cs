﻿using UnityEngine;
using System.Collections;
 
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class DropSlot : MonoBehaviour ,IBeginDragHandler, IDragHandler, IEndDragHandler,IDropHandler, IPointerEnterHandler, IPointerExitHandler, GameHandler.IGameHandler {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnDrop(PointerEventData eventData)
    {
        //Debug.Log("OnDrop to " + gameObject.name);
        Draggable d = eventData.pointerDrag.GetComponent<Draggable>();
        if (d != null)
        {

            d.parentToReturnto = this.transform;
            //d.gameObject.GetComponentInChildren<CardNetwork>().ChangeTargetCall(0, gameObject.name);
            //if (!d.gameObject.GetComponentInChildren<CardNetwork>().photonView.isMine)
            //{
            //    d.gameObject.transform.SetParent(CardHandler.Instance.OpponentCardsPlaceHolderList[0].transform);
            //    gameObject.ta
            //}

            Debug.Log("OnDrop to  DRop slot  " + this.gameObject.transform.tag);
        }
        //Debug.Log("OnDrop to  DRop slot outside " + this.gameObject.transform.tag);
        switch (this.gameObject.transform.tag)
        {
            case "drop1":
                Debug.Log("OnDrop to  DRop drop1  " + this.gameObject.transform.tag);
                CardLoaded(1,true);
                break;
            case "drop2":
                CardLoaded(2, true);
                Debug.Log("OnDrop to  DRop drop2  " + this.gameObject.transform.tag);
                break;
            case "drop3":
                Debug.Log("OnDrop to  DRop drop3 " + this.gameObject.transform.tag);
                CardLoaded(3, true);
                break;
            case "drop4":
                CardLoaded(4, true);
                break;
            case "drop5":
                CardLoaded(5, true);
                break;
            case "drop6":
                CardLoaded(6, true);
                break;
            case "drop7":
                CardLoaded(7, true);
                break;
        }



    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("OnBeginDrag DROP -------------------------------------------- SLOT");
        //  parentToReturnto = this.transform.parent;
        this.transform.SetParent(this.transform.parent.parent);
        GetComponent<CanvasGroup>().blocksRaycasts = false;
        switch (this.gameObject.transform.tag)
        {
            case "drop1":
                CardLoaded(1, false);

                break;
            case "Card2":
                CardLoaded(2, false);

                break;
            case "Card3":
                CardLoaded(3, false);

                break;
            case "Card4":
                CardLoaded(4, false);
                break;
            case "Card5":
                CardLoaded(5, false);
                break;
            case "Card6":
                CardLoaded(6, false);
                break;
            case "Card7":
                CardLoaded(7, false);
                break;

            //    //default:
            //    //    CardLoaded(1, false);
            //    //    CardLoaded(2, false);
            //    //    CardLoaded(3, false);
            //    //    CardLoaded(4, false);
            //    //    break;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {

        //Debug.Log("OnDrop to " + this.gameObject.name);


    }
    public void OnPointerClick(PointerEventData eventData)
    {

    }
   

    public void OnPointerExit(PointerEventData eventData)
    {
        //  Debug.Log("OnDrop to " + gameObject.name);



    }








    public void setCardDetails(Sprite imageSprite)
    {
        throw new System.NotImplementedException();
    }

    public void CardLoaded(int index, bool isloaded)
    {
        if (index == 1)
        {
            GameHandler.Instance.cardOneIsLoaded = isloaded;
        }
        if (index == 2)
        {
            GameHandler.Instance.cardTwoIsLoaded = isloaded;
        }
        if (index == 3)
        {
            GameHandler.Instance.cardThreeIsLoaded = isloaded;
        }
        if (index == 4)
        {
            GameHandler.Instance.cardFourIsLoaded = isloaded;
        }
        if (index == 5)
        {
            GameHandler.Instance.cardFiveIsLoaded = isloaded;
        }
        if (index == 6)
        {
            GameHandler.Instance.cardSixIsLoaded = isloaded;
        }
        if (index == 7)
        {
            GameHandler.Instance.cardSevenIsLoaded = isloaded;
        }
        if (GameHandler.Instance.cardOneIsLoaded && GameHandler.Instance.cardTwoIsLoaded && GameHandler.Instance.cardThreeIsLoaded && GameHandler.Instance.cardFourIsLoaded)
        {
            ReadyCards();
        //    Invoke("CheckHandSCore", 5f);
        }
        if (GameHandler.Instance.cardFiveIsLoaded && GameHandler.Instance.cardSixIsLoaded && GameHandler.Instance.cardSevenIsLoaded)
        {
            SecondRoundReadyCards();
        }
    }

    public void ReadyCards()
    {
           GUIHandler.Instance.ShowReadyBtn();
           Invoke("CheckHandSCore", 5f);
    }

    public void GameState()
    {
        throw new System.NotImplementedException();
    }

    public void CheckHandSCore()
    {
        GameHandler.Instance.CheckHandSCore();
    }

    public void SecondRoundReadyCards()
    {
        GameHandler.Instance.SecondRoundReadyCards();
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        throw new System.NotImplementedException();
    }

    public void OnDrag(PointerEventData eventData)
    {
        throw new System.NotImplementedException();
    }



   
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public int Index { get; set; }
    public bool cardOneIsLoaded;
    public bool cardTwoIsLoaded;
    public bool cardThreeIsLoaded;
    public bool cardFourIsLoaded;

    public bool cardFiveIsLoaded;
    public bool cardSixIsLoaded;

    public bool cardSevenIsLoaded;

    public int CardIndex { get; set; }
    public Text scoreText;
    public Animator ScoreAnimator;
    public int scoreValue;
    public Text opScoreText;
    public Sprite defaultSprite;
    public int ScoreValueOp;
    public GameObject colorPanel;
    public Text timerText;
    private PunTurnManager turnManager;
    public Sprite Black;
    public Sprite Blue;
    public Sprite Yellow;
    public Sprite Orange;
    public Sprite Purple;
    public Sprite Red;
    public Sprite Green;
    public Sprite Silver;
    public List<Sprite> colorsList = new List<Sprite>();
    public bool isOneColorReveled;
    public bool isTwoColorReveled;
    public Button playerReveledColorbtn;
    public Button OPReveledColorbtn;
    public int countBlack;
    public int countgreen;
    public int countRed;
    public int countYellow;
    public int countOrange;
    public int countSilver;
    public int countPurple;
    public Color oneColorReveled;
    public Color TwoColorReveled;

    public Button oneClrRevelbtn;
    public Button TwoClrRevelbtn;
    public Button OponeClrRevelbtn;
    public Button OponeTwoClrRevelbtn;

    public Image CardDetails;

    [SerializeField] private Image localSelectionImage;
    public Color localSelection = Color.Black;
    public List<Card> arrangeCardsList = new List<Card>();

    public Button colorSelectBtn;
    public Text plCardCountTxt;
    public Text pltwoCardCountText;
    public Text OptwoCardCountText;
    public Text opCardCountTxt;
    [SerializeField] private Image remoteSelectionImage;
    public Color remoteSelection;
    public GameStates CurrentGameState = GameStates.GameInit;
    public enum Color
    {
        Green,
        Yellow,
        Purple,
        Black,
        Silver,
        Blue,
        Orange,
        Red
    }
    public enum GameStates
    {
        GameInit,
        GameStart,
        GameColor,
        GameOver,
        GameWin,
        GameLost,
        GameFinish
    }

    void Awake()
    {
        colorsList.Add(Orange);
        colorsList.Add(Green);
        colorsList.Add(Blue);
        colorsList.Add(Black);
        colorsList.Add(Purple);
        colorsList.Add(Red);
        colorsList.Add(Yellow);
    }
    // Use this for initialization
    private void Start()
    {
        colorPanel.gameObject.SetActive(false);
        ColorReveled();
    }

    // Update is called once per frame
    private void Update()
    {

    }

    public void SecondRoundScore()
    {
      

         switch (CurrentGameState)
        {
            case  GameStates.GameInit:
                StartCoroutine(HighlightSpriteSecondRound());
                    break;
        }
        
    }
    public void ScoreCalculate()
    {
        StartCoroutine(HighlightSprite());
        CardManger.Instance.SecondRoundCardLoad();
        CardManger.Instance.SecondRoundAICardLoadIngame();
    }

    public IEnumerator HighlightSprite()
    {
        yield return null;

        for (var i = 0; i < 4; i++)
        {
            yield return new WaitForSeconds(1.5f);
            StartCoroutine( CardManger.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<CardS>().HighlightCard());
            StartCoroutine(CardManger.Instance.OpponentCardsPlaceHolderList[i].GetComponentInChildren<CardS>().HighlightCard());
            scoreValue +=CardManger.Instance.playerCardsPlaceHolderList[i].transform.GetChild(0).GetComponent<CardS>().value;
            scoreText.text = scoreValue.ToString();
            ScoreValueOp += CardManger.Instance.OpponentCardsList[i].GetComponent<CardS>().value;
            opScoreText.text = ScoreValueOp.ToString();
        }

        //   CardHandler.Instance.playerCardsList[0].GetComponent<Card>().bonusType =
        Debug.Log("HIghlighted sprite with score update");
        //CurrentGameState = GameStates.GameStart;

    }

    public IEnumerator HighlightSpriteSecondRound()
    {
        yield return null;

        for (var i = 4; i < 7; i++)
        {
            yield return new WaitForSeconds(1.5f);
            StartCoroutine(CardManger.Instance.playerCardsPlaceHolderList[i].GetComponentInChildren<CardS>().HighlightCard());
            StartCoroutine(CardManger.Instance.OpponentCardsPlaceHolderList[i].GetComponentInChildren<CardS>().HighlightCard());
            scoreValue += CardManger.Instance.playerCardsPlaceHolderList[i].transform.GetChild(0).GetComponent<CardS>().value;
            scoreText.text = scoreValue.ToString();
            ScoreValueOp += CardManger.Instance.OpponentCardsList[i].GetComponent<CardS>().value;
            opScoreText.text = ScoreValueOp.ToString();
        }

        //   CardHandler.Instance.playerCardsList[0].GetComponent<Card>().bonusType =
        Debug.Log("HIghlighted sprite with score update");
        //CurrentGameState = GameStates.GameStart;

    }

    public void ColorReveled()
    {
        var random = Random.Range(1, 3);
        var randomColor = Random.Range(0, 7);
        Debug.Log("Random value : " + random);
        if (random == 1)
        {
            isOneColorReveled = true;

            OneColorcardReveled(random);
            //    colorPanel.SetActive(true);
        }
        else
        {
            Debug.Log("Random value : " + random);
            isTwoColorReveled = true;

           
        }
    }
    public void OneColorcardReveled(int colorval)
    {
        isOneColorReveled = true;
        oneClrRevelbtn.image.sprite = colorsList[colorval];
        OponeClrRevelbtn.image.sprite = colorsList[colorval];
        Debug.Log("OneColorcardReveled: " + OponeClrRevelbtn.image.sprite.name);
        switch (OponeClrRevelbtn.image.sprite.name)
        {
            case "Silver":
                oneColorReveled = Color.Silver;
                Debug.Log("Silver");
                break;
            case "Red":
                oneColorReveled = Color.Red;
                Debug.Log("Red");
                break;
            case "Purple":
                oneColorReveled = Color.Purple;
                Debug.Log("Purple");
                break;
            case "Green":
                oneColorReveled = Color.Green;
                Debug.Log("Green");
                break;
            case "Blue":
                oneColorReveled = Color.Blue;
                Debug.Log("Blue");
                break;
            case "Yellow":
                oneColorReveled = Color.Yellow;
                Debug.Log("Yellow");
                break;
            case "Black":
                oneColorReveled = Color.Black;
                Debug.Log("Black");
                break;
        }
    }
    public void TwoColorcardReveled(int index)
    {
        isTwoColorReveled = true;
        playerReveledColorbtn.gameObject.SetActive(true);
        OPReveledColorbtn.gameObject.SetActive(true);


        playerReveledColorbtn.image.sprite = colorsList[index];
        // playerReveledColorbtn.image.sprite = colorsList[index+1];
        oneClrRevelbtn.image.sprite = colorsList[index + 1];

        TwoClrRevelbtn.image.sprite = colorsList[index];
        OponeTwoClrRevelbtn.image.sprite = colorsList[index];
        OPReveledColorbtn.image.sprite = colorsList[index];
        OponeClrRevelbtn.image.sprite = colorsList[index + 1];
        Debug.Log("Random TwoClrRevelbtns : " + OponeClrRevelbtn.image.sprite.name);
        switch (OponeTwoClrRevelbtn.image.sprite.name)
        {
            case "Silver":
                TwoColorReveled = Color.Silver;
                Debug.Log("Silver");
                break;
            case "Red":
                TwoColorReveled = Color.Red;
                Debug.Log("Red");
                break;
            case "Purple":
                TwoColorReveled = Color.Purple;
                Debug.Log("Purple");
                break;
            case "Green":
                TwoColorReveled = Color.Green;
                Debug.Log("Green");
                break;
            case "Blue":
                TwoColorReveled = Color.Blue;
                Debug.Log("Blue");
                break;
            case "Yellow":
                TwoColorReveled = Color.Yellow;
                Debug.Log("Yellow");
                break;
            case "Black":
                TwoColorReveled = Color.Black;
                Debug.Log("Black");
                break;
        }


        switch (OponeClrRevelbtn.image.sprite.name)
        {
            case "Silver":
                oneColorReveled = Color.Silver;
                Debug.Log("Silver");
                break;
            case "Red":
                oneColorReveled = Color.Red;
                Debug.Log("Red");
                break;
            case "Purple":
                oneColorReveled = Color.Purple;
                Debug.Log("Purple");
                break;
            case "Green":
                oneColorReveled = Color.Green;
                Debug.Log("Green");
                break;
            case "Blue":
                oneColorReveled = Color.Blue;
                Debug.Log("Blue");
                break;
            case "Yellow":
                oneColorReveled = Color.Yellow;
                Debug.Log("Yellow");
                break;
            case "Black":
                oneColorReveled = Color.Black;
                Debug.Log("Black");
                break;
        }
        Debug.Log("TwoClrRevelbtn: " + OPReveledColorbtn.image.sprite.name);
    }


}
    
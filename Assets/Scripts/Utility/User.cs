﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Utility
{
    public class User
    {
        private static User _instance;

        private User() { }

        public static User Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new User();
                return _instance;
            }
        }

        public int UserID { get; set; }
        public string Email { get; set; }
        public decimal Balance { get; set; }
        public string NickName { get; set; }
        public int RoomID { get; set; }
    }
}

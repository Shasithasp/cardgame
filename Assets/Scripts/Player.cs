﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public int id;
    public int position;
    public bool gotCards = false;

    [Header("UI")] public Image turnFlag;

    [SerializeField] public List<Card> PlayerCards;


    public Player()
    {
    }

    public Player(int id)
    {
        this.id = id;
        PlayerCards = new List<Card>(7);
    }


    public void Awake()
    {
         
    }


    public void StartTurnTimer()
    {
        StartCoroutine(StartTurnTimerIenu());
    }

    private IEnumerator StartTurnTimerIenu()
    {
        turnFlag.fillAmount = 0;
        while (turnFlag.fillAmount != 1)
        {
            //	turnFlag.fillAmount +=( 1.0f/GameHandler.Instance.turnTime) * Time.deltaTime;
            yield return null;
        }
        turnFlag.fillAmount = 1;
    }

    private void Update()
    {
    }
}

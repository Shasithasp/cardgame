using System.Reflection;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DropMe : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
	public Image containerImage;
	public Image receivingImage;
	private Color normalColor;
	public Color highlightColor = Color.yellow;
    public Sprite dropSprite;
    public GameObject panel;
	public void OnEnable ()
	{
		if (containerImage != null)
			normalColor = containerImage.color;
	}
	
	public void OnDrop(PointerEventData data)
	{
		//containerImage.color = normalColor;
		
		if (receivingImage == null)
			return;
        Debug.Log("OnDrop");
		Sprite dropSprite = GetDropSprite (data);
		if (dropSprite != null)
			receivingImage.overrideSprite = dropSprite;



	}

	public void OnPointerEnter(PointerEventData data)
	{
		if (containerImage == null)
			return;
		
		Sprite dropSprite = GetDropSprite (data);
		if (dropSprite != null)

			containerImage.color = highlightColor;
      //  data.selectedObject.transform.SetParent(this.gameObject.transform);
	 //   panel.GetComponent<Image>().sprite = dropSprite;
	}

	public void OnPointerExit(PointerEventData data)
	{
		if (containerImage == null)
			return;

		containerImage.color = normalColor;
        
	}
	
	private Sprite GetDropSprite(PointerEventData data)
	{
		var originalObj = data.pointerDrag;
     
		if (originalObj == null)
			return null;

		var srcImage = originalObj.GetComponent<Image>().sprite;
		if (srcImage == null)
			return null;
		
		return srcImage;
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using MonoBehaviour = Photon.MonoBehaviour;
public class CardNetwork : MonoBehaviour {


    //    ThirdPersonCamera cameraScript;
    //    ThirdPersonController controllerScript;

    private void Awake()
    {
        //        cameraScript = GetComponent<ThirdPersonCamera>();
        //        controllerScript = GetComponent<ThirdPersonController>();

        if (photonView.isMine)
        {
            //MINE: local player, simply enable the local scripts
            //           cameraScript.enabled = true;
            //           controllerScript.enabled = true;

        }
        else
        {
          //  gameObject.transform.SetParent(CardHandler.Instance.OpponentCardsPlaceHolderList.Add(this.gameObject.transform ));
            //gameObject.GetComponent<Image>().sprite = gameObject.GetComponent<Card>().sprite;
            CardHandler.Instance.OpponentCardsList.Add(gameObject);
           
        }
        //gameObject.transform.localScale = new Vector3(3f, 3f);

        gameObject.name = gameObject.name + photonView.viewID;
    }

    public void SwapSpriteCall()
    {
        if (photonView.isMine)
        {
            photonView.RPC("HighlightSprite", PhotonTargets.AllBuffered);
        }
    }

    [PunRPC]
    private void SwapSprite()
    {
      //  StartCoroutine(CardHandler.Instance.ChangeSprite(gameObject, 0.5f));
    }

    public void ChangeCardpos()
    {
        if (!photonView.isMine)
        {
            
        }
    }
    public void ChangeTargetCall(string name)
    {
        //Debug.Log("CHANGE TARGET CALL : :"+v3.x +v3.y);
        if (photonView.isMine)
        {
            photonView.RPC("ChangeTarget", PhotonTargets.AllBuffered, name);
        }
    }

    [PunRPC]
    private void ChangeTarget(string name)
    {
        //var target = new Vector3(v3.x, v3.y, v3.z);

        foreach (var cardname in CardHandler.Instance.playerCardsPlaceHolderList)
        {
            //if (cardname.transform.GetChild(0).name ==name)
            //{
            ////    this.gameObject.GetComponent<Card>().transform.SetParent(CardHandler.Instance.OpponentCardsPlaceHolderList.Add(this.gameObject));
            //}
        }
      //  gameObject.GetComponent<Card>().target = target;

        //Debug.Log("ChangeTarget " + target);
    }

    public void DrawCardToPlayerCall()
    {
        if (photonView.isMine)
        {
            photonView.RPC("DrawCardToPlayer", PhotonTargets.AllBuffered);
        }
    }

    [PunRPC]
    private void DrawCardToPlayer()
    {
       CardHandler.Instance.SetCardTargetLocally(gameObject, photonView.ownerId);
    Debug.Log("DrawCardToPlayer");
    }

    private void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            //We own this player: send the others our data
             stream.SendNext(gameObject.GetComponent<Card>().setTargetLocally);
            stream.SendNext(gameObject.GetComponent<Card>().id);
            stream.SendNext(gameObject.GetComponent<Card>().value);
            //			if (gameObject.GetComponent<Card> ().isSyncPosAndRot) {
            //				stream.SendNext (transform.position);
            //				stream.SendNext (transform.rotation); 
            //			}
        }
        else
        {
            //			//Network player, receive data
                 gameObject.GetComponent<Card>().setTargetLocally = (bool)stream.ReceiveNext();
                   gameObject.GetComponent<Card>().id = (int)stream.ReceiveNext();
           // gameObject.GetComponent<Card>().sprite.name = (string)stream.ReceiveNext();
            //			if (gameObject.GetComponent<Card> ().isSyncPosAndRot) {
            //				correctPlayerPos = (Vector3)stream.ReceiveNext ();
            //				correctPlayerRot = (Quaternion)stream.ReceiveNext ();
            //			}
        }
    }

    //    private Vector3 correctPlayerPos = Vector3.zero; //We lerp towards this
    //    private Quaternion correctPlayerRot = Quaternion.identity; //We lerp towards this
  
    private void Update()
    {


        //        if (!photonView.isMine)
        //        {
        //            //Update remote player (smooth this, this looks good, at the cost of some accuracy)
        //            transform.position = Vector3.Lerp(transform.position, correctPlayerPos, Time.deltaTime * 5);
        //            transform.rotation = Quaternion.Lerp(transform.rotation, correctPlayerRot, Time.deltaTime * 5);
        //        }
    }
}

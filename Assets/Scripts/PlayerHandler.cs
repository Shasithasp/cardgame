﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class PlayerHandler :  Singleton<PlayerHandler>
{
    protected PlayerHandler()
    {
    }
   
    public List<Player> playerObjList = new List<Player>();
    public Transform playerPrefab;
    public Transform playerInitTransform;
    public List<GameObject> playerList = new List<GameObject>();
    public List<GameObject> playerPlaceHolderList = new List<GameObject>();
    public GameObject you;
    public int initialChildCount;
 

    private readonly List<List<int>> orderList = new List<List<int>>();
    private readonly List<int> two = new List<int> {0, 1};
    //private readonly List<int> three = new List<int> {0, 2, 4};
    //private readonly List<int> four = new List<int> {0, 2, 4, 6};
    //private readonly List<int> five = new List<int> {0, 2, 4, 5, 6};
    //private readonly List<int> six = new List<int> {0, 1, 2, 4, 5, 6};
    //private readonly List<int> seven = new List<int> {0, 1, 2, 3, 4, 5, 6};
    //private readonly List<int> eight = new List<int> {0, 1, 2, 3, 4, 5, 6, 7};

    public void Awake()
    {
        if (!PhotonNetwork.connected)
        {
            SceneManager.LoadScene(CardGameRoomMenu.SceneNameMenu);
        }
    }

   
    public virtual void OnPhotonRandomJoinFailed()
    {
        Debug.Log("OnPhotonRandomJoinFailed() was called by PUN. No random room available, so we create one. Calling: PhotonNetwork.CreateRoom(my, new RoomOptions() {maxPlayers = 4}, null);");
        PhotonNetwork.CreateRoom("my", new RoomOptions() { MaxPlayers = 2 ,PlayerTtl = 5000}, null);
    }
 
    private void Start()
    {

 
        orderList.Add(two);
        //orderList.Add(three);
        //orderList.Add(four);
        //orderList.Add(five);
        //orderList.Add(six);
        //orderList.Add(seven);
        //orderList.Add(eight);
        DrawPlayer();
        
    }

    // Use this for initialization
    public void DrawPlayer()
    {
      

        var go = PhotonNetwork.Instantiate(playerPrefab.name, Vector3.zero, Quaternion.identity, 0);
        DontDestroyOnLoad(go);
 
        go.transform.SetParent(playerInitTransform);
        go.transform.localScale = new Vector3(1, 1, 1);
        go.name = PhotonNetwork.player.ID + "-You";
  
        go.GetComponent<Player>().id = PhotonNetwork.player.ID;
 
    
        go.GetComponent<Player>().position = 0;
        initialChildCount = go.transform.childCount;
        you = go;
        playerList.Add(go);
    }

  

    public void AddPlayer(GameObject go, PhotonView photonView)
    {
     go.transform.position = playerPlaceHolderList [playerList.Count].transform.position;

        go.transform.SetParent(playerInitTransform);
        go.transform.localScale = new Vector3(1, 1, 1);
        var photonPlayer = PhotonNetwork.player;
        photonPlayer.name = PhotonNetwork.playerName;

        DontDestroyOnLoad(go);

        #region 

        //Hashtable hashtable = new Hashtable();
        //hashtable[1] = photonPlayer.ID + "-" + photonPlayer.name;
        //hashtable[2] = go.GetComponent<Player>().playerName;
        //photonPlayer.SetCustomProperties(hashtable);

        #endregion

        
        foreach (var item in PhotonNetwork.playerList)
        {
            if (item.ID == photonView.ownerId)
                photonPlayer = item;
        }

        go.name = photonPlayer.ID + "-" + photonPlayer.name;
      
        go.GetComponent<Player>().id = photonPlayer.ID;
        //   go.GetComponent<Image>().sprite = photonView.GetComponent<Player>().PlayerAvatar;
        go.GetComponent<Player>().position = playerList.Count;
        playerList.Add(go);
        SetPositions();
    }


    public GameObject GetPlayer(int id)
    {
        foreach (var item in playerList)
        {
            if (item.GetComponent<Player>().id == id)
                return item;
        }
        return new GameObject();
    }


    private void SetPositions()
    {
        playerList.Sort(delegate(GameObject firstObj,
            GameObject secondObj)
        {
            return getIDFromEntry(firstObj).CompareTo(getIDFromEntry(secondObj));
        }
            );
        foreach (var item in orderList)
        {
            if (playerList.Count == item.Count)
            {
                var index = playerList.FindIndex(a => a.GetComponent<Player>().id == you.GetComponent<Player>().id);
                for (var i = 0; i < item.Count; i++)
                {
                    playerList[index].transform.position = playerPlaceHolderList[item[i]].transform.position;
                    playerList[index].GetComponent<Player>().position = item[i];
                    index++;
                    if (index >= playerList.Count)
                        index = 0;
                }
            }
        }
    }

    public List<int> GetPlayerIDs()
    {
        playerList.Sort(delegate(GameObject firstObj,
            GameObject secondObj)
        {
            return getIDFromEntry(firstObj).CompareTo(getIDFromEntry(secondObj));
        }
            );
        var ids = new List<int>();
        foreach (var item in playerList)
        {
            ids.Add(item.GetComponent<Player>().id);
        }
        return ids;
    }

    private int getIDFromEntry(GameObject obj)
    {
        return obj.GetComponent<Player>().id;
    }

    private void OnPhotonPlayerConnected(PhotonPlayer photonPlayer)
    {
        //Debug.Log("PlayerHandler:OnPhotonPlayerConnected " + photonPlayer.ID + "-" + photonPlayer.name);
        var player = new Player();
        //player.playerName = photonPlayer.name;
        player.id = photonPlayer.ID;
 

        playerObjList.Add(player);


 
    }

    
    private void OnPhotonPlayerDisconnected(PhotonPlayer photonPlayer)
    {
        var go = new GameObject();
        foreach (var item in playerList)
        {
            if (item == null || photonPlayer.ID == item.GetComponent<Player>().id)
                go = item;
        }

        playerList.Remove(go);
 
    }

    private void Update()
    {
    }

  
}
